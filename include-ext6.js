/**
 * This file includes the required ext-all js and css files based upon "theme" and "rtl"
 * url parameters.  It first searches for these parameters on the page url, and if they
 * are not found there, it looks for them on the script tag src query string.
 * For example, to include the neptune flavor of ext from an index page in a subdirectory
 * of extjs/examples/:
 * <script type="text/javascript" src="../../examples/shared/include-ext.js?theme=neptune"></script>
 */
(function() {
    function getQueryParam(name) {
        var regex = RegExp('[?&]' + name + '=([^&]*)');

        var match = regex.exec(location.search) || regex.exec(scriptPath);
        return match && decodeURIComponent(match[1]);
    }

    function hasOption(opt, queryString) {
        var s = queryString || location.search;
        var re = new RegExp('(?:^|[&?])' + opt + '(?:[=]([^&]*))?(?:$|[&])', 'i');
        var m = re.exec(s);

        return m ? (m[1] === undefined || m[1] === '' ? true : m[1]) : false;
    }

    function loadCss(url) {
        document.write('<link rel="stylesheet" type="text/css" href="' + url + '"/>');
    }

    function loadScript(url, defer) {
        document.write('<script type="text/javascript" src="' + url + '"' +
                (defer ? ' defer' : '') + '></script>');
    }

    Ext = window.Ext || {};

    // The value of Ext.repoDevMode gets replaced during a build - do not change this line
    // 2 == internal dev mode, 1 == external dev mode, 0 == build mode
    Ext.devMode = 1;

    var scriptEls = document.getElementsByTagName('script'),
        scriptPath = scriptEls[scriptEls.length - 1].src,
        rtl = getQueryParam('rtl'),
        includeCSS = !hasOption('nocss', scriptPath),
        useDebug = hasOption('debug', scriptPath),
        profile = getQueryParam('profile') || Ext.profile || 'classic',
        themeName = getQueryParam('theme') || 'crisp',
        themes = {
            default: 'triton',
            modern: ['ios','material','neptune','triton'],
            classic: ['aria','classic','classic-sandbox','crisp','crisp-touch','gray','neptune','neptune-touch','triton']
        },
        themeName = (themes[profile].indexOf(themeName) > 0) ? themeName : themes.default,

        extDir = (getQueryParam('path') || (() => {
            s=scriptPath.split('/');
            for(let i=1;i<=3;i++) s.shift();
            s.pop();

            return s.join('/');
        })())+'/build',

        rtlSuffix = (rtl ? '-rtl' : ''),
        debugSuffix = (useDebug ? '-debug' : ''),
        cssSuffix = rtlSuffix + debugSuffix + '.css',
        themePackageDir, chartsJS, uxJS, themeOverrideJS, extPrefix;

    rtl = rtl && rtl.toString() === 'true';
    uxJS = extDir + '/packages/ux/'+profile+'/ux' + debugSuffix + '.js';
    chartsJS = extDir + '/packages/charts/'+profile+'/charts' + debugSuffix + '.js';
    themePackageDir = extDir + '/'+profile+'/theme-' + themeName + '/';

    if (includeCSS) {
        let theme = themeName.replace('-touch','');
        loadCss(themePackageDir + 'resources/theme-' + themeName + '-all' + cssSuffix);
        loadCss(extDir + '/packages/charts/'+profile+'/' + ((profile == 'modern')?'modern-':'') + theme + '/resources/charts-all' + cssSuffix);

        // Of all the modern themes, only neptune has a ux-all CSS file
        if (profile == 'modern') theme == 'neptune' && loadCss(extDir + '/packages/ux/modern/modern-' + theme + '/resources/ux-all' + cssSuffix);
        else loadCss(extDir + '/packages/ux/classic/' + theme + '/resources/ux-all' + cssSuffix);
    }

    extPrefix = '/ext-'+((profile == 'modern')?'modern-':'')+'all' + debugSuffix;
    loadScript(extDir + extPrefix + rtlSuffix + '.js');

    // since document.write('<script>') does not block execution in IE, we need to
    // make sure we prevent theme overrides from executing before ext-all.js
    // normally this can be done using the defer attribute on the script tag, however
    // this method does not work in IE when in repoDevMode.  It seems the reason for
    // this is because in repoDevMode ext-all.js is simply a script that loads other
    // scripts and so Ext is still undefined when the neptune overrides are executed.
    // To work around this we use the _beforereadyhandler hook to load the theme
    // overrides dynamically after Ext has been defined.
    themeOverrideJS = themePackageDir + 'theme-' + themeName + debugSuffix + '.js';

    if (useDebug) {
        if (window.ActiveXObject) {
            Ext = {
                _beforereadyhandler: function() {
                    Ext.Loader.loadScript({url: themeOverrideJS});
                }
            };
        }
    }

    loadScript(themeOverrideJS, true);
    loadScript(uxJS);
    loadScript(chartsJS);
})();
