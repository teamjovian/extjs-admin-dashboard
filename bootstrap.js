/** 
 * JovianLib MicroLoader V2
 * 
 * <p>A micro loader that'll dynamically load a JSON file with JavaScript, CSS, 
 * meta tags, and various other application files and load them into memory.
 * 
 * This version of the MicroLoader is compatible with ES6 or higher and won't 
 * work on browsers or JavaScript engines that are on a previos version such 
 * as IE11 or below.</p>
 * 
 * Usage:
 *      var loader = new MicroLoader();
 *      loader.load('app.json');
 * 
 * @author    Aaron Snyder <aaronrsnyder@gmail.com>
 * @date      2017-06-27
 * @version   2.1.0
 * @revision  1
 * @package   Jovian\Core
 * @copyright 2017 Jovian Project
 */
class MicroLoader {
    /**
     * Clones an object
     * @param {Object} item The object to clone
     * @return {Object} The new object
     * @private
     */
    static $clone(item) {
        var toString = Object.prototype.toString,
            enumerables = [//'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable',
                   'valueOf', 'toLocaleString', 'toString', 'constructor'];

        if (item === null || item === undefined) return item;

        var type = toString.call(item), i, j, k, clone, key;
        if (type === '[object Date]') return new Date(item.getTime());
        if (type === '[object Array]') {
            clone = [];
            var temp = [].concat(item);
            i = temp.length;
            while (i--) { clone[i] = $clone(temp[i]); }
        }

        // Object
        else if (type === '[object Object]' && item.constructor === Object) {
            clone = {};
            for (key in item) { clone[key] = MicroLoader.$clone(item[key]); }
            if (enumerables) {
                for (j = enumerables.length; j--;) {
                    k = enumerables[j];
                    if (item.hasOwnProperty(k)) clone[k] = item[k];
                }
            }
        }

        return clone || item;
    }

    /**
     * Applies the values to the specified object
     * @param {Object} o The target object
     * @param {Object} c The object containing the values to be applied
     * @param {Object} defaults (optional) An object containing default 
     * values to apply to the target if te container doesn't contain them
     * @return {Object} The target object
     * @private
     */
    static $apply(o, c, defaults) {
        if (defaults) MicroLoader.$apply(o, defaults);
        if (o && c && typeof c == 'object') {
            var m = MicroLoader.$clone(c);
            for (var p in c) o[p] = m[p];
        }
        return o;
    }

    /**
     * Gets value for the specified query parameter in the URL
     * @param {String} name The param name
     * @return {String} The value
     * @private
     */
    static $getQueryParam (name, path) {
        var regex = RegExp('[?&]' + name + '=([^&]*)');

        var match = regex.exec(location.search) || regex.exec(path);
        return match && decodeURIComponent(match[1]);
    }

    /**
     * Generates a randome string/code of the specified length
     * @param {Number} len (optional) The length of the code to generate
     * @param {String} type (optional) The Type of code to generate
     */
    static $gencode(len = 6, pool = 'gen') {
        var pools = {
            gen: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
            alpha: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        };

        var args = [...arguments], str = [];

        switch (args.length) {
            case 1:
                if (typeof args[0] == 'number') len = args[0];
                else pool = args[0];
                break;

            case 2:
                len = args[0];
                pool = args[1];
                break;
        }

        for (let i = 0; i < Number(len); i++) {
            let r = pools[pool].substr(Math.round(pools[pool].length * Math.random()), 1);
            if (!String(r).length)--i;
            else str.push(r);
        }

        return str.join('');
    }

    /**
     * Determines if the browser or platform is a phone
     * @param {String} ua The browser userAgent
     * @return {Boolean} TRUE if the device is a phone, FALSE otherwise
     * @private
     */
    static $isPhone (ua) {
        var isMobile = /Mobile(\/|\s)/.test(ua);

        // Either:
        // - iOS but not iPad
        // - Android 2
        // - Android with "Mobile" in the UA

        return /(iPhone|iPod)/.test(ua) ||
            (!/(Silk)/.test(ua) && (/(Android)/.test(ua) && (/(Android 2)/.test(ua) || isMobile))) ||
            (/(BlackBerry|BB)/.test(ua) && isMobile) ||
            /(Windows Phone)/.test(ua);
    }

    /**
     * Determines if the browser or platform is a tablet
     * @param {String} ua The browser userAgent
     * @return {Boolean} TRUE if the device is a tablet, FALSE otherwise
     * @private
     */
    static $isTablet (ua) {
        return !MicroLoader.$isPhone(ua) && (/iPad/.test(ua) || /Android|Silk/.test(ua) || /(RIM Tablet OS)/.test(ua) ||
            (/MSIE 10/.test(ua) && /; Touch/.test(ua)));
    }

    /**
     * Platform detection
     * @param {String} platform The platform to test for
     * @return {Boolean} TRUE if running on the specified platform, FALSE otherwise
     * @private
     */
    static $filterPlatform (platform) {
        var profileMatch = false,
            ua = navigator.userAgent,
            j, jln;

        platform = [].concat(platform);

        // Check if the ?platform parameter is set in the URL
        var paramsString = window.location.search.substr(1),
            paramsArray = paramsString.split("&"),
            params = {},
            testPlatform, i;

        for (i = 0; i < paramsArray.length; i++) {
            var tmpArray = paramsArray[i].split("=");
            params[tmpArray[0]] = tmpArray[1];
        }

        testPlatform = params.platform;
        if (testPlatform) {
            return platform.indexOf(testPlatform) != -1;
        }

        for (j = 0, jln = platform.length; j < jln; j++) {
            switch (platform[j]) {
                case 'phone': profileMatch = MicroLoader.$isPhone(ua); break;
                case 'tablet': profileMatch = MicroLoader.$isTablet(ua); break;
                case 'mobile': profileMatch = MicroLoader.$isPhone(ua) || MicroLoader.$isTablet(ua); break;
                case 'desktop': profileMatch = !MicroLoader.$isPhone(ua) && !MicroLoader.$isTablet(ua); break;
                case 'ios': profileMatch = /(iPad|iPhone|iPod)/.test(ua); break;
                case 'android': profileMatch = /(Android|Silk)/.test(ua); break;
                case 'blackberry': profileMatch = /(BlackBerry|BB)/.test(ua); break;
                case 'safari': profileMatch = /Safari/.test(ua) && !(/(BlackBerry|BB)/.test(ua)); break;
                case 'chrome': profileMatch = /Chrome/.test(ua); break;
                case 'ie': profileMatch = /MSIE/.test(ua); break;
                case 'ie10': profileMatch = /MSIE 10/.test(ua); break;
                case 'ie11': profileMatch = /MSIE 11/.test(ua); break;
                case 'edge': profileMatch = /Edge/.test(ua); break;
                case 'windows': profileMatch = /MSIE/.test(ua) || /Trident/.test(ua) || /Windows/.test(ua); break;
                case 'tizen': profileMatch = /Tizen/.test(ua); break;
                case 'firefox': profileMatch = /Firefox/.test(ua); break;
            }

            if (profileMatch) return true;
        }
        return false;
    }

    /**
     * Loads the specified JSON file and turn it into an Object
     * @param {String} jsonFile The file to load
     * @param {Function} callback A function to override the options properties
     * @return {Object} The new options object
     * @private
     */
    static $loadFile(jsonFile, fn, callback, async = true) {
        var xhr = new XMLHttpRequest(),
            callback = callback || function() {};

        xhr.onreadystatechange = () => { if (xhr && xhr.readyState == 4) {
            try { var options = eval("(" + xhr.responseText + ")"); }
            catch(e) {
                var options = {};
                console.warn('MicroLoader: Unable to process file ' + jsonFile); 
            }

            fn(callback(options));
        }}

        xhr.open('GET', jsonFile, async);
        xhr.send(null);
    }

    /**
     * Writes to the document body
     * @param {String} content The content to write
     * @private
     */
    static $write (content) {
        document.write(content);
    }

    /**
     * Setup properties used to write to the DOM
     */
    constructor() {
        MicroLoader.$apply(this, {
            $head: document.head || document.getElementsByTagName('head')[0],
            $body: document.body || document.getElementsByTagName('body')[0],
            $scriptEls: document.getElementsByTagName('script')
        });

        this.$scriptPath = this.$scriptEls[this.$scriptEls.length - 1].src


        // Sets up a cross browser onReady method for deferring 
        // script load until the DOM is ready
        if (typeof MicroLoader.$defer == 'undefined') MicroLoader.$defer = (function () {
            // https://github.com/jfriend00/docReady
            //"use strict";
            var readyList = [];
            var readyFired = false;
            var readyEventHandlersInstalled = false;
            function ready() {
                if (!readyFired) {
                    readyFired = true;
                    for (var i = 0; i < readyList.length; i++) {
                        readyList[i].fn.call(window, readyList[i].ctx);
                    }
                    readyList = [];
                }
            }

            function readyStateChange() {
                if (document.readyState === "complete") ready();
            }

            return function $defer(callback, context) {
                if (readyFired) {
                    setTimeout(function () { callback(context); }, 1);
                    return;
                } else readyList.push({ fn: callback, ctx: context });
                if (document.readyState === "complete" || (!document.attachEvent && document.readyState === "interactive")) {
                    setTimeout(ready, 1);
                } else if (!readyEventHandlersInstalled) {
                    if (document.addEventListener) {
                        document.addEventListener("DOMContentLoaded", ready, false);
                        window.addEventListener("load", ready, false);
                    } else {
                        document.attachEvent("onreadystatechange", readyStateChange);
                        window.attachEvent("onload", ready);
                    }
                    readyEventHandlersInstalled = true;
                }
            }
        })();

        this.$restrictViewport();
    }

    /**
     * Do some mobile detection and setup the viewport as fullscreen if it's mobile
     * @private
     */
    $restrictViewport () {
        if (MicroLoader.$filterPlatform('mobile')) {
            if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
                var msViewportStyle = document.createElement("style");
                msViewportStyle.appendChild(
                    document.createTextNode(
                        "@media screen and (orientation: portrait) {" +
                        "@-ms-viewport {width: 320px !important;}" +
                        "}" +
                        "@media screen and (orientation: landscape) {" +
                        "@-ms-viewport {width: 560px !important;}" +
                        "}"
                    )
                );
                document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
            }

            this.$addMeta('viewport', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no');
            this.$addMeta('apple-mobile-web-app-capable', 'yes');
            this.$addMeta('apple-touch-fullscreen', 'yes');
            this.$addMeta('HandheldFriendly', 'true');
        }
    }

    /**
     * Adds JavaScript script tags to the document
     * @param {String} path The path to the file to load
     * @param {String|function} onload JavaScript to execute as part of the script tags <i>onload</i> event
     * @param {Boolean} writeJs TRUE to write a script tag to the document, FALSE to append a 
     * script element; defaults to FALSE
     * @private
     */
    $addJs (path, onload, writeJs) {
        var onload = onload || function () { },
            writeJs = (writeJs === false) ? false : true;

        if (typeof onload == 'string') {
            try { var onload = eval("(" + onload + ")"); }
            catch(e) {
                console.warn('Unable to evaluate script onload function string');
            }            
        }

        if (writeJs === true) MicroLoader.$write('<script ' + [
                'type="text/javascript"',
                'src="' + path + '"',
                'onload="var x = (' + onload.toString() + ')(\''+path+'\')"'
            ].join(' ') + '></script>');

        else {
            onload = onload.bind(this, path);
            var js = document.createElement("script");

            js.setAttribute('type', 'text/javascript');
            js.setAttribute('src', path);

            if (MicroLoader.$filterPlatform('ie8')) {
                js.addEventListener('readystatechange', function () {
                    if (this.readyState == 'loaded') { onload(); };
                });
            }

            else js.addEventListener('load', onload);

            this.$head.appendChild(js);
        }
    }

    /**
     * Adds meta content tags to the document
     * @param {String|Object} name The value for the name attribute or an object with a name/value pair to use instead of the name attribute
     * @param {String} content The value for the content attribute  
     * @private
     */
    $addMeta (name, content) {
        var meta = document.createElement('meta');

        if (typeof name == 'string') meta.setAttribute('name', name);
        else meta.setAttribute(name.name, name.value);
        meta.setAttribute('content', content);
        this.$head.appendChild(meta);
    }

    /**
     * Creates a path with a cache control variable to prevent caching the resource
     * @param {String} path The path to apply the cache control to
     * @param {String|Boolean} type The type of cache control; possible values are never, version, or FALSE
     * @private
     */
    $cacheControl(path, type) {
        var tmpPath, tmpQuery;

        if (typeof type != 'undefined' && type !== false) {
            tmpPath = path.split('?');
            if (tmpPath.length == 1) {
                tmpPath.push('cache=' + ((type == 'never')
                    ? MicroLoader.$gencode(10)
                    : this.version));

                path = tmpPath.join('?');
            }

            else {
                tmpQuery = tmpPath[1].split('&');
                tmpQuery.push('cache=' + ((type == 'never')
                    ? MicroLoader.$gencode(10)
                    : this.version));

                tmpQuery = tmpQuery.join('&');
                tmpPath[1] = tmpQuery;
                path = tmpPath.join('?');
            }
        }

        return path;        
    }

    /**
     * Create meta tags from the specified meta tag objects
     * @param {Array} meta An array of meta tag objects
     */
    createMetaTags (meta) {
        var meta = meta || [],
            i, ln, platform, metaname, exclude, metatag, content;

        // Add configured meta tags
        for (i = 0, ln = meta.length; i < ln; i++) {
            metatag = meta[i];

            if (typeof metatag != 'string') {
                platform = metatag.platform;
                exclude = metatag.exclude;
                metaname = metatag.name;
                content = metatag.content;
            }

            if (platform) {
                if (!MicroLoader.$filterPlatform(platform) || MicroLoader.$filterPlatform(exclude)) {
                    continue;
                }
            }

            this.$addMeta(metaname, content);
        }
    }
    
    /**
     * Load configured style sheets
     * @param {Array} styleSheets An array of style sheet objects
     */
    loadStyleSheets (styleSheets, cacheControl) {
        var styleSheets = styleSheets || [],
            i, ln, path, platform, theme, exclude;

        for (i = 0, ln = styleSheets.length; i < ln; i++) {
            var path = styleSheets[i];

            if (typeof path != 'string')
                var platform = path.platform,
                    exclude = path.exclude,
                    theme = path.theme,
                    path = path.path;

            if (platform) {
                if (!MicroLoader.$filterPlatform(platform) || MicroLoader.$filterPlatform(exclude)) {
                    continue;
                }
            }

            path = this.$cacheControl(path, cacheControl);
            MicroLoader.$write('<link rel="stylesheet" href="' + path + '" />');
        }
    }

    /**
     * Load configured JavaScript files
     * @param {Array} scripts An array of JavaScript file objects
     */
    loadScripts(scripts, cacheControl) {
        var scripts = scripts || [],
            i, ln, path, platform, theme, exclude, onload;

        for (i = 0, ln = scripts.length; i < ln; i++) {
            path = scripts[i];

            if (typeof path != 'string') {
                platform = path.platform;
                exclude = path.exclude;
                onload = path.onload;
                path = path.path;
            }

            if (platform) {
                if (!MicroLoader.$filterPlatform(platform) || MicroLoader.$filterPlatform(exclude)) {
                    continue;
                }
            }

            path = this.$cacheControl(path, cacheControl);
            this.$addJs(path, onload);
            //MicroLoader.$write('<script type="text/javascript" src="' + path + '"></script>');
        }
    }

    /**
     * Load configured deferred JavaScript files
     * @param {Array} scripts An array of JavaScript file objects
     */
    loadDeferredScripts (scripts, cacheControl) {
        var self = this,
            scripts = scripts || [],
            i, ln, path, platform, theme, exclude, onload;

        for (i = 0, ln = scripts.length; i < ln; i++) {
            path = scripts[i];

            if (typeof path != 'string') {
                platform = path.platform;
                exclude = path.exclude;
                onload = path.onload;
                path = path.path;
            }

            if (platform) {
                if (!MicroLoader.$filterPlatform(platform) || MicroLoader.$filterPlatform(exclude)) {
                    continue;
                }
            }

            MicroLoader.$defer(() => {
                path = this.$cacheControl(path, cacheControl);
                self.$addJs(path, onload, false);
            });
        }
    }

    /**
     * Callback that's fired when the script file is loaded in order to 
     * override any of the properties in the objections object.  This 
     * should be overridden and return the options object.
     * @param {Object} The options object
     * @return {Object} The options object with the overrides
     * @private
     */
    onFileLoad (options) {
        return options;
    }

    /**
     * Load and process the specified JSON file
     * @param {String|Object} o (optional) The JSON file to process or an 
     * already loaded object with the file contents; defaults to 'app.json'
     * and can be overridden by the config property in the query path if 
     * not specified.
     */
    load (o, async = true) {
        // If the options object or script file isn't specified, find it
        // in the query path or default to app.js
        if (typeof o == 'undefined') {
            var c = MicroLoader.$getQueryParam('config', this.$scriptPath);
            o = (c) ? c : 'app.json';
        }

        // Loads in the file list if options is a file path and not an object
        if (typeof o == 'string') MicroLoader.$loadFile(o, (o) => {
            this.manifest = o;
            var scripts = o.js || [],
                cacheControl = o.cacheControl || false,
                styles = o.css || [],
                styleSheets = [],
                deferredSheets = [],
                deferred = o.defer || [],
                meta = o.meta || [];

            // Adds in optional script tags that are specific to the loaded platform
            if (o.platform && o.platforms && o.platforms[o.platform] && o.platforms[o.platform].js) {
                scripts = o.platforms[o.platform].js.concat(scripts);
            }

            for(let i = 0; i < styles.length; i++) 
                if (typeof styles[i].defer != 'undefined' &&
                    styles[i].defer === true) deferredSheets.push(styles[i]);
                else styleSheets.push(styles[i]);
            
            // Set the page title if it's present in the config object
            if (typeof o.name == 'string') 
                MicroLoader.$write('<title>'+o.name+'</title>');

            this.version = (typeof o.version == 'undefined') ? '1.0.0' : o.version;
            
            this.createMetaTags(meta);
            this.loadStyleSheets(styleSheets, cacheControl);
            this.loadScripts(scripts, cacheControl);
            this.loadDeferredScripts(deferred, cacheControl);
            if (deferredSheets.length) this.loadStyleSheets(deferredSheets, cacheControl);
        }, this.onFileLoad.bind(this), async);
    }

};