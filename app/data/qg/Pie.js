Ext.define('Shared.data.qg.Pie', {
    extend: 'Shared.data.Simulated',

    data: [{
        'xvalue': 'Research',
        'yvalue': 68
    }, {
        'xvalue': 'Finance',
        'yvalue': 20
    }, {
        'xvalue': 'Marketing',
        'yvalue': 12
    }]
});