Ext.define('Shared.data.Simulated', {
    extend: 'Ext.Base',

    onClassExtended: function (e, b) {
        var d = b.$className.toLowerCase().replace(/\./g, '/').replace(/^shared\/data/, '~api'),
        c = {
            type: 'json',
            data: b.data
        },
        a = {};
        a[d] = c;
        Ext.ux.ajax.SimManager.register(a)
    }
}, function () {
    Ext.ux.ajax.SimManager.init({
        defaultSimlet: null
    });
});