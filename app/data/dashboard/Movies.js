Ext.define('Shared.data.dashboard.Movies', {
    extend: 'Shared.data.Simulated',

    data: [{
        'xvalue': 'Foo',
        'yvalue': 943
    }, {
        'xvalue': 'Bar',
        'yvalue': 622
    }, {
        'xvalue': 'Baz',
        'yvalue': 1044
    }]
});