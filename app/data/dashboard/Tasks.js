Ext.define('Shared.data.dashboard.Tasks', {
    extend: 'Shared.data.Simulated',

    data: [{
        'id': 1,
        'task': 'Upgrade to SSD harddisks',
        'done': !0
    }, {
        'id': 2,
        'task': 'Pay server invoice',
        'done': !0
    }, {
        'id': 3,
        'task': 'Upgrade to SSD harddisks',
        'done': !1
    }, {
        'id': 4,
        'task': 'Pay server invoice',
        'done': !1
    }, {
        'id': 5,
        'task': 'Upgrade to SSD harddisks',
        'done': !1
    }]
});