Ext.define('Shared.data.search.Users', {
    extend: 'Shared.data.Simulated',

    data: [{
        'identifier': 1,
        'fullname': 'Archie Young',
        'profile_pic': '1.png',
        'email': 'dwatkins@mydeo.name',
        'subscription': 'minima',
        'joinDate': '10/16/2012',
        'isActive': !1
    }, {
        'identifier': 2,
        'fullname': 'May Williams',
        'profile_pic': '2.png',
        'email': 'jreid@babbleblab.com',
        'subscription': 'ab',
        'joinDate': '6/13/2004',
        'isActive': !0
    }, {
        'identifier': 3,
        'fullname': 'Kathryn Hill',
        'profile_pic': '4.png',
        'email': 'dwatkins@mydeo.name',
        'subscription': 'totam',
        'joinDate': '2/2/2007',
        'isActive': !0
    }, {
        'identifier': 4,
        'fullname': 'Katherine Gomez',
        'profile_pic': '3.png',
        'email': 'ewatkins@dazzlesphere.biz',
        'subscription': 'alias',
        'joinDate': '2/22/2002',
        'isActive': !0
    }, {
        'identifier': 5,
        'fullname': 'Della Allen',
        'profile_pic': '1.png',
        'email': 'vgonzalez@yamia.gov',
        'subscription': 'et',
        'joinDate': '1/16/2001',
        'isActive': !0
    }, {
        'identifier': 6,
        'fullname': 'Maude Bailey',
        'profile_pic': '2.png',
        'email': 'jgreene@skalith.com',
        'subscription': 'inventore',
        'joinDate': '7/23/2006',
        'isActive': !0
    }, {
        'identifier': 7,
        'fullname': 'Alma Allen',
        'profile_pic': '4.png',
        'email': 'dwalker@jatri.info',
        'subscription': 'quo',
        'joinDate': '11/24/2011',
        'isActive': !0
    }, {
        'identifier': 8,
        'fullname': 'Floyd Taylor',
        'profile_pic': '3.png',
        'email': 'ewatkins@dazzlesphere.biz',
        'subscription': 'eaque',
        'joinDate': '12/11/2010',
        'isActive': !0
    }, {
        'identifier': 9,
        'fullname': 'Archie Reed',
        'profile_pic': '1.png',
        'email': 'dwalker@jatri.info',
        'subscription': 'sapiente',
        'joinDate': '7/22/2001',
        'isActive': !0
    }, {
        'identifier': 10,
        'fullname': 'Stanley Brooks',
        'profile_pic': '1.png',
        'email': 'foliver@twitterbeat.org',
        'subscription': 'sed',
        'joinDate': '10/3/2012',
        'isActive': !1
    }, {
        'identifier': 11,
        'fullname': 'Beatrice Miller',
        'profile_pic': '1.png',
        'email': 'walexander@meevee.name',
        'subscription': 'nemo',
        'joinDate': '2/25/2003',
        'isActive': !1
    }, {
        'identifier': 12,
        'fullname': 'Leon Jones',
        'profile_pic': '4.png',
        'email': 'jreid@babbleblab.com',
        'subscription': 'est',
        'joinDate': '7/14/2009',
        'isActive': !1
    }]
});