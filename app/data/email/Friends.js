Ext.define('Shared.data.email.Friends', {
    extend: 'Shared.data.Simulated',

    data: [{
        'id': 0,
        'online': !0,
        'name': 'Torres Tran'
    }, {
        'id': 1,
        'online': !1,
        'name': 'Oneill Franklin'
    }, {
        'id': 2,
        'online': !1,
        'name': 'Branch Allison'
    }, {
        'id': 3,
        'online': !0,
        'name': 'Hines Moon'
    }, {
        'id': 4,
        'online': !0,
        'name': 'Molina Wilkerson'
    }, {
        'id': 5,
        'online': !0,
        'name': 'Suzette Powell'
    }]
});