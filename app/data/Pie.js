Ext.define('Shared.data.Pie', {
    extend: 'Shared.data.Simulated',

    data: [{
        'xvalue': 'Drama',
        'yvalue': 10
    }, {
        'xvalue': 'Fantasy',
        'yvalue': 10
    }, {
        'xvalue': 'Action',
        'yvalue': 12
    }]
});