Ext.define('Shared.model.faq.Category', {
    extend: 'Shared.model.Base',

    fields: [{
        type: 'string',
        name: 'name'
    }],

    hasMany: {
        name: 'questions',
        model: 'faq.Question'
    }
});