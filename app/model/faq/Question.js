Ext.define('Shared.model.faq.Question', {
    extend: 'Shared.model.Base',
    
    fields: [{
        type: 'string',
        name: 'name'
    }]
});