Ext.define('Shared.model.FriendsList', {
    extend: 'Shared.model.Base',
    
    fields: [
        { name: 'friendsName' },
        { name: 'connectionStatus' }
    ]
});