Ext.define('Shared.model.ChatMessages', {
    extend: 'Shared.model.Base',

    fields: [{
        type: 'string',
        name: 'message'
    }, {
        type: 'string',
        defaultValue: 'user',
        name: 'sender'
    }]
});