Ext.define('Shared.model.search.Attachment', {
    extend: 'Shared.model.Base',
    
    fields: [
        { type: 'int', name: 'id' },
        { type: 'string', name: 'url' },
        { type: 'string', name: 'title' }
    ]
});