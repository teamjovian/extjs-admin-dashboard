Ext.define('Shared.model.Base', {
    extend: 'Ext.data.Model',
    
    schema: {
        namespace: 'Shared.model'
    }
});