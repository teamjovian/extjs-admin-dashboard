Ext.define('Shared.model.DataXY', {
    extend: 'Shared.model.Base',

    fields: [
        { name: 'xvalue' },
        { name: 'yvalue' }
    ]
});