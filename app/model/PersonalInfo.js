Ext.define('Shared.model.PersonalInfo', {
    extend: 'Shared.model.Base',
    
    fields: [
        { name: 'name' },
        { name: 'status' },
        { name: 'icon' }
    ]
});