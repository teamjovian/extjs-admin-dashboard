Ext.define('Shared.model.email.Friend', {
    extend: 'Shared.model.Base',
    
    fields: [
        { type: 'int', name: 'id' }, 
        { type: 'string', name: 'name' }, 
        { type: 'string', name: 'thumbnail' }, 
        { type: 'boolean', name: 'online' }
    ]
});