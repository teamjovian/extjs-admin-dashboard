Ext.define('Shared.model.Subscription', {
    extend: 'Shared.model.Base',
    
    fields: [
        { type: 'int', name: 'id' },
        { type: 'string', name: 'name' },
        { type: 'string', name: 'subscription' }
    ]
});