Ext.define('Shared.model.YearwiseData', {
    extend: 'Shared.model.Base',
    
    fields: [
        { name: 'year' },
        { name: 'data' }
    ]
});