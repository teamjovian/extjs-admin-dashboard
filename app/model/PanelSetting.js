Ext.define('Shared.model.PanelSetting', {
    extend: 'Shared.model.Base',
    
    fields: [
        { name: 'title' },
        { name: 'subTitle' },
        { name: 'toggleStatus' }
    ]
});
