Ext.define('Shared.store.email.Friends', {
    extend: 'Ext.data.Store',
    alias: 'store.emailfriends',

    requires: [
        'Shared.data.email.Friends'
    ],

    model: 'Shared.model.email.Friend',
    autoLoad: !0,
    proxy: {
        type: 'api',
        url: '~api/email/friends'
    },
    sorters: {
        direction: 'DESC',
        property: 'online'
    }
});
