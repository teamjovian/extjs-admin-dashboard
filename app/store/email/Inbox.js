Ext.define('Shared.store.email.Inbox', {
    extend: 'Ext.data.Store',
    alias: 'store.inbox',

    requires: [
        'Shared.data.email.Inbox'
    ],

    model: 'Shared.model.email.Email',
    pageSize: 20,
    autoLoad: !0,
    proxy: {
        type: 'api',
        url: '~api/email/inbox'
    }
});