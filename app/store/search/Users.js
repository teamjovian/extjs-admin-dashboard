Ext.define('Shared.store.search.Users', {
    extend: 'Ext.data.Store',
    alias: 'store.searchusers',

    requires: [
        'Shared.data.search.Users'
    ],

    model: 'Shared.model.search.User',
    proxy: {
        type: 'api',
        url: '~api/search/users'
    },
    autoLoad: 'true',
    sorters: {
        direction: 'ASC',
        property: 'fullname'
    }
});