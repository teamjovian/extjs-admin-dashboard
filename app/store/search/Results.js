Ext.define('Shared.store.search.Results', {
    extend: 'Ext.data.Store',
    alias: 'store.searchresults',

    requires: [
        'Shared.data.search.Results'
    ],

    model: 'Shared.model.search.Results',
    proxy: {
        type: 'api',
        url: '~api/search/results'
    },
    autoLoad: 'true',
    sorters: {
        direction: 'ASC',
        property: 'title'
    }
});