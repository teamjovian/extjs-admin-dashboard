Ext.define('Shared.store.faq.FAQ', {
    extend: 'Ext.data.Store',
    alias: 'store.faq',

    requires: [
        'Shared.data.faq.FAQ'
    ],

    model: 'Shared.model.faq.Category',
    proxy: {
        type: 'api',
        url: '~api/faq/faq'
    }
});