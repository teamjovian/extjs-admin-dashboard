Ext.define('Shared.view.profile.Timeline', {
    extend: 'Ext.DataView',
    alias: 'widget.profiletimeline',
    // xtype: 'profiletimeline',

    cls: 'timeline-items-wrap',
    scrollable: !1,
    bind: '{userTimeline}',
    itemSelector: '.timeline-item',
    
    itemTpl: ['<div class="timeline-item{userId:this.cls(values,parent[xindex-2],xindex-1,xcount)}">{date:this.epoch(values,parent[xindex-2],xindex-1,xcount)}<div class="profile-pic-wrap"><img src="resources/images/user-profile/{userId}.png" alt="Smiley face"><div>{date:this.elapsed} ago</div></div><tpl if="notificationType == \'image_sharing\'"><div class="line-wrap"><div class="contents-wrap"><div class="shared-by"><a href="#">{name}</a> shared an image</div><img src="resources/images/img2.jpg" class="shared-img" alt="Smiley face"></div></div><tpl elseif="notificationType == \'job_meeting\'"><div class="line-wrap"><div class="contents-wrap"><div class="job-meeting"><a href="#">Job Meeting</a></div><div>{content}</div></div></div><tpl elseif="notificationType == \'comment_reply\'"><div class="line-wrap"><div class="contents-wrap"><div class="shared-by"><a href="#">{name}</a> commented on The Article</div><div class="article-comment"><span class="x-fa fa-quote-left"></span>{content}</div></div></div><tpl elseif="notificationType == \'new_follower\'"><div class="line-wrap"><div class="contents-wrap"><div class="followed-by"><img src="resources/images/user-profile/10.png" alt="Smiley face"><div class="followed-by-inner"><a href="#">{name}</a> followed you.</div></div></div></div><tpl elseif="notificationType == \'comment\'"><div class="line-wrap"><div class="contents-wrap"><div class="shared-by"><a href="#">Lorem ipsum dolor sit amet</a></div><div>{content}</div></div></div><tpl elseif="notificationType == \'like\'"><div class="line-wrap"><div class="contents-wrap"><div class="followed-by"><img src="resources/images/user-profile/1.png" alt="Smiley face"><div class="followed-by-inner"><a href="#">{name}</a> Like The Article.</div></div></div></tpl></div>', {
        cls: function (f, e, d, b, c) {
            var a = '';
            if (!b) {
                a += ' timeline-item-first'
            }
            if (b + 1 === c) {
                a += ' timeline-item-last'
            }
            return a
        },
        elapsed: function (j) {
            var i = Date.now();
            i = +new Date('2015/08/23 21:15:00');
            var f = Math.floor((i - j) / 1000),
            d = Math.floor(f / 60),
            c = Math.floor(d / 60),
            b = Math.floor(c / 24),
            g = Math.floor(b / 7),
            e = Math.floor(b / 30),
            h = Math.floor(b / 365),
            a;
            e %= 12;
            g %= 52;
            b %= 365;
            c %= 24;
            d %= 60;
            f %= 60;
            if (h) {
                a = this.part(h, 'Year');
                a += this.part(e, 'Month', ' ')
            } else {
                if (e) {
                    a = this.part(e, 'Month');
                    a += this.part(b, 'Day', ' ')
                } else {
                    if (g) {
                        a = this.part(g, 'Week');
                        a += this.part(b, 'Day', ' ')
                    } else {
                        if (b) {
                            a = this.part(b, 'Day');
                            a += this.part(c, 'Hour', ' ')
                        } else {
                            if (c) {
                                a = this.part(c, 'Hour')
                            } else {
                                if (d) {
                                    a = this.part(d, ' Minute')
                                } else {
                                    a = this.part(f, 'Second')
                                }
                            }
                        }
                    }
                }
            }
            return a
        },
        epoch: function (f, d, a, b, e) {
            var c = a && (a.isModel ? a.data : a)['date'];
            if (b === 4) {
                return '<div class="timeline-epoch">Yesterday</div>'
            }
            return ''
        },
        part: function (a, c, d) {
            var b = a ? (d || '') + a + ' ' + c : '';
            if (a > 1) {
                b += 's'
            }
            return b
        }
    }]
});