Ext.define('Shared.view.profile.UserProfileModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.userprofile',

    requires: [
        'Shared.data.UserTimeline',
        'Shared.data.UserSharedItems'
    ],

    stores: {
        userSharedItems: {
            autoLoad: !0,
            fields: [
                { name: '_id' },
                { name: 'parent_id' },
                { name: 'name' },
                { name: 'source' },
                { name: 'date' },
                { name: 'isActive' },
                { name: 'time' },
                { name: 'content' }
            ],
            proxy: {
                type: 'api',
                url: '~api/usershareditems'
            }
        },
        userTimeline: {
            autoLoad: !0,
            fields: [
                { name: '_id' },
                { name: 'name' },
                { name: 'content' },
                { name: 'date', type: 'date' },
                { name: 'userId' },
                { name: 'notificationType' }
            ],
            proxy: {
                type: 'api',
                url: '~api/usertimeline'
            }
        }
    }
});