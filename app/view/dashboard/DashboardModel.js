Ext.define('Shared.view.dashboard.DashboardModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.dashboard',

    requires: [
        'Shared.model.DataXY',
        'Shared.model.MultiDataXY',
        'Shared.model.Subscription',

        'Shared.data.qg.Area',
        'Shared.data.qg.Bar',
        'Shared.data.qg.Line',
        'Shared.data.qg.Pie',

        'Shared.data.dashboard.Counce',
        'Shared.data.dashboard.Full',
        'Shared.data.dashboard.Movies',
        'Shared.data.dashboard.Tasks',
        'Shared.data.dashboard.Visitor',

        'Shared.data.Subscriptions'
    ],

    stores: {
        hddusage: {
            autoLoad: !0,
            model: 'Shared.model.DataXY',
            proxy: {
                type: 'api',
                url: '~api/qg/area'
            }
        },
        quarterlyGrowth: {
            autoLoad: !0,
            model: 'Shared.model.DataXY',
            proxy: {
                type: 'api',
                url: '~api/qg/bar'
            }
        },
        earnings: {
            autoLoad: !0,
            model: 'Shared.model.DataXY',
            proxy: {
                type: 'api',
                url: '~api/qg/line'
            }
        },
        servicePerformance: {
            autoLoad: !0,
            model: 'Shared.model.DataXY',
            proxy: {
                type: 'api',
                url: '~api/qg/pie'
            }
        },
        topMovies: {
            autoLoad: !0,
            model: 'Shared.model.DataXY',
            proxy: {
                type: 'api',
                url: '~api/dashboard/movies'
            }
        },
        networkData: {
            autoLoad: !0,
            model: 'Shared.model.MultiDataXY',
            proxy: {
                type: 'api',
                url: '~api/dashboard/full'
            }
        },
        visitors: {
            autoLoad: !0,
            model: 'Shared.model.MultiDataXY',
            proxy: {
                type: 'api',
                url: '~api/dashboard/visitor'
            }
        },
        bounces: {
            autoLoad: !0,
            model: 'Shared.model.MultiDataXY',
            proxy: {
                type: 'api',
                url: '~api/dashboard/counce'
            }
        },
        subscriptions: {
            autoLoad: !0,
            model: 'Shared.model.Subscription',
            proxy: {
                type: 'api',
                url: '~api/subscriptions'
            }
        },
        todos: {
            autoLoad: !0,
            fields: [
                { type: 'int', name: 'id' }, 
                { type: 'string', name: 'task' }, 
                { type: 'boolean', name: 'done' }
            ],
            proxy: {
                type: 'api',
                url: '~api/dashboard/tasks'
            }
        }
    }
});