Ext.define('Shared.view.dashboard.DashboardController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.dashboard',

    onRefreshToggle: function (c, e, d) {
        var a,
        b;
        if (c.toggleValue) {
            this.clearChartUpdates()
        } else {
            a = this.getStore('networkData');
            if (a.getCount()) {
                b = this.chartTaskRunner;
                if (!b) {
                    this.chartTaskRunner = b = new Ext.util.TaskRunner()
                }
                b.start({
                    run: function () {
                        var b = a.first();
                        a.remove(b);
                        a.add(b)
                    },
                    interval: 200
                })
            }
        }
        c.toggleValue = !c.toggleValue
    },

    clearChartUpdates: function () {
        this.chartTaskRunner = Ext.destroy(this.chartTaskRunner)
    },

    destroy: function () {
        this.clearChartUpdates();
        Ext.app.ViewController.prototype.destroy.call(this)
    },
    
    onHideView: function () {
        this.clearChartUpdates()
    }
});