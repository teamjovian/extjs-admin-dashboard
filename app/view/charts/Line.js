Ext.define('Shared.view.charts.Line', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartslinepanel',
    // xtype: 'chartslinepanel',

    title: 'Line Chart',
    iconCls: 'x-fa fa-line-chart',
    items: [{
        xtype: 'cartesian',
        downloadServerUrl: Shared.Config.downloadServerUrl,
        colors: ['#6aa5db', '#94ae0a'],
        bind: '{lineData}',
        axes: [{
            type: 'category',
            fields: ['xvalue'],
            hidden: !0,
            position: 'bottom'
        }, {
            type: 'numeric',
            fields: ['yvalue', 'y1value', 'y2value', 'y3value', 'y4value', 'y5value'],
            hidden: !0,
            position: 'left'
        }],
        series: [{
            type: 'line',
            xField: 'xvalue',
            yField: ['yvalue']
        }, {
            type: 'line',
            xField: 'xvalue',
            yField: ['y1value']
        }],
        platformConfig: {
            phone: {
                touchAction: {
                    panX: !0,
                    panY: !0
                }
            },
            '!phone': {
                interactions: {
                    type: 'panzoom',
                    zoomOnPanGesture: !0
                }
            }
        }
    }]
});