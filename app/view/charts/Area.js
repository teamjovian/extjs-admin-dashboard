Ext.define('Shared.view.charts.Area', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartsareapanel',
    // xtype: 'chartsareapanel',

    title: 'Area Chart',
    iconCls: 'x-fa fa-area-chart',
    items: [{
        xtype: 'cartesian',
        downloadServerUrl: Shared.Config.downloadServerUrl,
        colors: ['#115fa6', '#94ae0a'],
        bind: '{areaData}',
        series: [{
            type: 'line',
            colors: ['rgba(103, 144, 199, 0.6)'],
            xField: 'xvalue',
            yField: ['y1value'],
            fill: !0,
            smooth: !0
        }, {
            type: 'line',
            colors: ['rgba(238, 146, 156, 0.6)'],
            xField: 'xvalue',
            yField: ['y2value'],
            fill: !0,
            smooth: !0
        }],
        platformConfig: {
            phone: {
                touchAction: {
                    panX: !0,
                    panY: !0
                }
            },
            '!phone': {
                interactions: {
                    type: 'panzoom',
                    zoomOnPanGesture: !0
                }
            }
        },
        axes: [{
            type: 'category',
            fields: ['xvalue'],
            hidden: !0,
            position: 'bottom'
        }, {
            type: 'numeric',
            fields: ['y1value', 'y2value', 'y3value'],
            grid: {
                odd: {
                    fill: '#e8e8e8'
                }
            },
            hidden: !0,
            position: 'left'
        }]
    }]
});