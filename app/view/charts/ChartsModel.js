Ext.define('Shared.view.charts.ChartsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.charts',

    requires: [
        'Shared.model.DataXY',
        'Shared.model.MultiDataXY',

        'Shared.data.marketshare.OneYear',
        'Shared.data.marketshare.MultiYear',
        'Shared.data.marketshare.OneEntity',
        'Shared.data.marketshare.Yearwise',

        'Shared.data.Pie',
        'Shared.data.Radial',
    ],

    stores: {
        barData: {
            model: 'Shared.model.DataXY',
            autoLoad: !0,
            proxy: {
                type: 'api',
                url: '~api/marketshare/oneyear'
            }
        },
        stackedData: {
            model: 'Shared.model.MultiDataXY',
            autoLoad: !0,
            proxy: {
                type: 'api',
                url: '~api/marketshare/multiyear'
            }
        },
        gaugeData: {
            data: [{ position: 40 }],
            fields: [{ name: 'position' }]
        },
        radialData: {
            model: 'Shared.model.DataXY',
            autoLoad: !0,
            proxy: {
                type: 'api',
                url: '~api/radial'
            }
        },
        lineData: {
            model: 'Shared.model.DataXY',
            autoLoad: !0,
            proxy: {
                type: 'api',
                url: '~api/marketshare/oneentity'
            }
        },
        pieData: {
            model: 'Shared.model.DataXY',
            autoLoad: !0,
            proxy: {
                type: 'api',
                url: '~api/pie'
            }
        },
        areaData: {
            model: 'Shared.model.MultiDataXY',
            autoLoad: !0,
            proxy: {
                type: 'api',
                url: '~api/dashboard/full'
            }
        }
    }
});