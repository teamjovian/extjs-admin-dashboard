Ext.define('Shared.view.charts.Network', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.chartnetwork',
    // xtype: 'chartnetwork',

    animation: !Ext.isIE9m && Ext.os.is.Desktop,
    downloadServerUrl: Shared.Config.downloadServerUrl,
    insetPadding: 0,
    axes: [{
        type: 'category',
        fields: ['xvalue'],
        hidden: !0,
        position: 'bottom'
    }, {
        type: 'numeric',
        fields: ['y1value', 'y2value'],
        grid: {
            odd: {
                fill: '#e8e8e8'
            }
        },
        hidden: !0,
        position: 'left'
    }],
    series: [{
        type: 'line',
        colors: ['rgba(103, 144, 199, 0.6)'],
        useDarkerStrokeColor: !1,
        xField: 'xvalue',
        yField: 'y1value',
        fill: !0,
        smooth: !0
    }, {
        type: 'line',
        colors: ['rgba(238, 146, 156, 0.6)'],
        useDarkerStrokeColor: !1,
        xField: 'xvalue',
        yField: 'y2value',
        fill: !0,
        smooth: !0
    }],
    interactions: [{
        type: 'panzoom'
    }]
});