Ext.define('Shared.view.charts.Pie', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartspiepanel',
    // xtype: 'chartspiepanel',

    title: '2D Pie Chart',
    iconCls: 'x-fa fa-pie-chart',
    items: [{
        xtype: 'polar',
        colors: ['#aed581', '#6aa5db', '#ee929c'],
        downloadServerUrl: Shared.Config.downloadServerUrl,
        bind: '{pieData}',
        series: [{
            type: 'pie',
            label: {
                field: 'xvalue',
                display: 'rotate',
                contrast: !0,
                font: '12px Open Sans',
                color: '#888'
            },
            xField: 'yvalue'
        }],
        platformConfig: {
            '!phone': {
                interactions: 'rotate'
            }
        }
    }]
});