Ext.define('Shared.view.charts.Polar', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartspolarpanel',
    // xtype: 'chartspolarpanel',

    title: 'Radial Chart',
    iconCls: 'x-fa fa-dot-circle-o',
    items: [{
        xtype: 'polar',
        colors: ['#6aa5db'],
        bind: '{radialData}',
        downloadServerUrl: Shared.Config.downloadServerUrl,
        axes: [{
            type: 'numeric',
            fields: ['yvalue'],
            grid: !0,
            position: 'radial'
        }, {
            type: 'category',
            fields: ['xvalue'],
            grid: !0,
            position: 'angular'
        }],
        series: [{
            type: 'radar',
            xField: 'xvalue',
            yField: 'yvalue'
        }],
        platformConfig: {
            phone: {
                touchAction: {
                    panX: !0,
                    panY: !0
                }
            },
            '!phone': {
                interactions: 'rotate'
            }
        }
    }]
});