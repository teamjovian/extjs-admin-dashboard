Ext.define('Shared.view.charts.Visitors', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.chartvisitors',
    // xtype: 'chartvisitors',

    animation: !Ext.isIE9m && Ext.os.is.Desktop,
    height: 22,
    background: 'rgba(255, 255, 255, 1)',
    colors: ['rgba(225,233,244, 0.8)'],
    downloadServerUrl: Shared.Config.downloadServerUrl,
    insetPadding: {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    axes: [{
        type: 'category',
        fields: ['xvalue'],
        hidden: !0,
        position: 'bottom'
    }, {
        type: 'numeric',
        fields: ['y1value'],
        grid: {
            odd: {
                fill: '#e8e8e8'
            }
        },
        hidden: !0,
        position: 'left'
    }],
    series: [{
        type: 'area',
        xField: 'xvalue',
        yField: ['y1value']
    }],
    interactions: [{
        type: 'panzoom'
    }]
});