Ext.define('Shared.view.charts.Pie3D', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartspie3dpanel',
    // xtype: 'chartspie3dpanel',

    title: '3D Pie Chart',
    iconCls: 'x-fa fa-pie-chart',
    items: [{
        xtype: 'polar',
        colors: ['#aed581', '#6aa5db', '#ee929c'],
        downloadServerUrl: Shared.Config.downloadServerUrl,
        platformConfig: {
            phone: {
                touchAction: {
                    panX: !0,
                    panY: !0
                }
            },
            '!phone': {
                interactions: 'rotate'
            }
        },
        bind: '{pieData}',
        series: [{
            type: 'pie3d',
            angleField: 'yvalue',
            donut: 30
        }]
    }]
});