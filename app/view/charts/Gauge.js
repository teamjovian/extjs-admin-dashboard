Ext.define('Shared.view.charts.Gauge', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartsgaugepanel',
    // xtype: 'chartsgaugepanel',

    title: 'Gauge Chart',
    iconCls: 'x-fa fa-wifi',
    items: [{
        xtype: 'polar',
        colors: ['#6aa5db', '#aed581'],
        bind: '{gaugeData}',
        downloadServerUrl: Shared.Config.downloadServerUrl,
        series: [{
            type: 'gauge',
            angleField: 'position',
            needleLength: 100
        }],
        platformConfig: {
            phone: {
                touchAction: {
                    panX: !0,
                    panY: !0
                }
            }
        }
    }]
});