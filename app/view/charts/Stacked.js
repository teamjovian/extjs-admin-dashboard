Ext.define('Shared.view.charts.Stacked', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartsstackedpanel',
    // xtype: 'chartsstackedpanel',

    title: 'Stacked Bar Chart',
    iconCls: 'x-fa fa-bar-chart',
    items: [{
        xtype: 'cartesian',
        colors: ['#6aa5db', '#ee929c'],
        bind: '{stackedData}',
        downloadServerUrl: Shared.Config.downloadServerUrl,
        axes: [{
            type: 'category',
            fields: ['xvalue'],
            hidden: !0,
            position: 'bottom'
        }, {
            type: 'numeric',
            fields: ['y1value', 'y2value', 'y3value'],
            grid: {
                odd: {
                    fill: '#e8e8e8'
                }
            },
            hidden: !0,
            position: 'left'
        }],
        series: [{
            type: 'bar',
            xField: 'xvalue',
            yField: ['y2value', 'y3value']
        }],
        platformConfig: {
            phone: {
                touchAction: {
                    panX: !0,
                    panY: !0
                }
            },
            '!phone': {
                interactions: {
                    type: 'panzoom',
                    zoomOnPanGesture: !0
                }
            }
        }
    }]
});