Ext.define('Shared.view.charts.Bar', {
    extend: 'Shared.view.charts.ChartBase',
    alias: 'widget.chartsbarpanel',
    // xtype: 'chartsbarpanel',

    title: 'Bar Chart',
    iconCls: 'x-fa fa-bar-chart',
    items: [{
        xtype: 'cartesian',
        downloadServerUrl: Shared.Config.downloadServerUrl,
        colors: ['#6aa5db'],
        bind: '{barData}',
        axes: [{
            type: 'category',
            fields: ['xvalue'],
            hidden: !0,
            position: 'bottom'
        }, {
            type: 'numeric',
            fields: ['yvalue'],
            grid: {
                odd: {
                    fill: '#e8e8e8'
                }
            },
            hidden: !0,
            position: 'left'
        }],
        series: [{
            type: 'bar',
            xField: 'xvalue',
            yField: ['yvalue']
        }],
        platformConfig: {
            phone: {
                touchAction: {
                    panX: !0,
                    panY: !0
                }
            },
            '!phone': {
                interactions: {
                    type: 'panzoom',
                    zoomOnPanGesture: !0
                }
            }
        }
    }]
});