Ext.define('Shared.Config', {
    singleton: true,

    // This was created to prevent the "Sencha's download server" warning 
    // when using the default download server for draw containers while 
    // loading the ext-*-all-debug.js file. Explicitly setting this value 
    // will surpress the warning
    downloadServerUrl: 'http://svg.sencha.io'
});