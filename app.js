Ext.Loader.setPath({
    'App': '/app',
    'Shared': '/app'
});

Ext.application({
	requires: [
		'Shared.Config'
	],
	
	name: 'Admin',
	appFolder: Ext.profile,
	extend: 'Admin.Application'
});