Ext.define('Admin.Application', {
    extend: 'Ext.app.Application',

    requires: [
        'Shared.proxy.API'
    ],

    controllers: [
        'Admin'
    ],
    
    name: 'Admin',
    stores: ['NavigationTree'],
    appFolder: 'classic',
    defaultToken: 'dashboard',
    mainView: 'Admin.view.main.Main',

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?', function (a) {
            if (a === 'yes') {
                window.location.reload()
            }
        })
    }
});
