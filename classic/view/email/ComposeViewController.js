Ext.define('Admin.view.email.ComposeViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emailcompose',

	onComposeDiscardClick: function (b) {
		var a = b.up('window');
		if (a) {
			a.close()
		}
	}
});