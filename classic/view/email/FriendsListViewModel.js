Ext.define('Admin.view.email.FriendsListViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.emailfriendslist',

	stores: {
		friends: {
			type: 'emailfriends',
			autoLoad: !0
		}
	}
});