Ext.define('Admin.view.email.Menu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.emailmenu',
    // xtype: 'emailmenu',

    requires: [
        'Admin.view.email.MenuViewModel'
    ],

    viewModel: {
        type: 'emailmenu'
    },
    title: 'Email',
    iconCls: 'x-fa fa-inbox',
    floating: !1,
    items: [{
        routeId: 'emailcompose',
        params: {
            openWindow: !0,
            targetCfg: {},
            windowCfg: {
                title: 'Compose Message'
            }
        },
        iconCls: 'x-fa fa-edit',
        text: 'Compose'
    }, {
        routeId: '',
        iconCls: 'x-fa fa-inbox',
        text: 'Inbox'
    }, {
        routeId: '',
        iconCls: 'x-fa fa-check-circle',
        text: 'Sent Mail'
    }, {
        routeId: '',
        iconCls: 'x-fa fa-exclamation-circle',
        text: 'Spam'
    }, {
        routeId: '',
        iconCls: 'x-fa fa-trash-o',
        text: 'Trash'
    }]
});