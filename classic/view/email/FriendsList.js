Ext.define('Admin.view.email.FriendsList', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.emailfriendslist',
    // xtype: 'emailfriendslist',

    requires: [
        'Admin.view.email.FriendsListViewController',
        'Admin.view.email.FriendsListViewModel'
    ],

	viewModel: {
		type: 'emailfriendslist'
	},
	controller: 'emailfriendslist',
	title: 'Friends',
	cls: 'navigation-email',
	iconCls: 'x-fa fa-group',
	floating: !1
});