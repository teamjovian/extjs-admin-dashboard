Ext.define('Admin.view.email.FriendsListViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.emailfriendslist',

	init: function () {
		var b = this,
		a = b.getViewModel().getStore('friends');
		a.on('load', function (a) {
			a.sort()
		});
		a.on('sort', function (a) {
			b.mutateData(a, a.getRange())
		});
		Ext.app.ViewController.prototype.init.apply(this, arguments)
	},

	mutateData: function (f, b) {
		var c = this.getView(),
		d = [],
		e = b.length,
		a;
		for (a = 0; a < e; a++) {
			d.push({
				xtype: 'menuitem',
				text: b[a].get('name'),
				cls: 'font-icon ' + (b[a].get('online') ? 'online-user' : 'offline-user')
			})
		}
		Ext.suspendLayouts();
		c.removeAll(!0);
		c.add(d);
		Ext.resumeLayouts(!0)
	}
});