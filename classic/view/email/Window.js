Ext.define('Admin.view.email.Window', {
    extend: 'Ext.window.Window',
    alias: 'widget.emailwindow',
    // xtype: 'emailwindow',

	autoShow: !0,
	modal: !0,
	layout: 'fit',
	width: 200,
	height: 200,

	afterRender: function () {
		var a = this;
		Ext.window.Window.prototype.afterRender.apply(this, arguments);
		a.syncSize();
		Ext.on(a.resizeListeners = {
				resize: a.onViewportResize,
				scope: a,
				buffer: 50
			})
	},

	doDestroy: function () {
		Ext.un(this.resizeListeners);
		Ext.window.Window.prototype.doDestroy.call(this)
	},

	onViewportResize: function () {
		this.syncSize()
	},

	syncSize: function () {
		var b = Ext.Element.getViewportWidth(),
		a = Ext.Element.getViewportHeight();
		this.setSize(Math.floor(b * 0.9), Math.floor(a * 0.9));
		this.setXY([Math.floor(b * 0.05), Math.floor(a * 0.05)])
	}
});