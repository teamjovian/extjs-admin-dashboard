Ext.define('Admin.view.email.EmailController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.email',

	init: function () {
		this.setCurrentView('inbox')
	},

	onBackBtnClick: function () {
		this.setCurrentView('inbox')
	},

	onMenuClick: function (b, a) {
		if (a && a.routeId === 'emailcompose') {
			this.setCurrentView(a.routeId, a.params)
		}
	},

	setCurrentView: function (c, b) {
		var a = this.getView().down('#contentPanel');
		if (!a || c === '' || a.down() && a.down().xtype === c) {
			return !1
		}
		if (b && b.openWindow) {
			var d = Ext.apply({
					xtype: 'emailwindow',
					items: [Ext.apply({
							xtype: c
						}, b.targetCfg)]
				}, b.windowCfg);
			Ext.create(d)
		} else {
			Ext.suspendLayouts();
			a.removeAll(!0);
			a.add(Ext.apply({
					xtype: c
				}, b));
			Ext.resumeLayouts(!0)
		}
	},

	onGridCellItemClick: function (c, d, b, a) {
		if (b > 1) {
			this.setCurrentView('emaildetails', {
				record: a
			})
		} else {
			if (b === 1) {
				a.set('favorite', !a.get('favorite'))
			}
		}
	},

	beforeDetailsRender: function (a) {
		var b = a.record ? a.record : {};
		a.down('#mailBody').setHtml(b.get('contents'));
		a.down('#attachments').setData(b.get('attachments'));
		a.down('#emailSubjectContainer').setData(b.data ? b.data : {});
		a.down('#userImage').setSrc('resources/images/user-profile/' + b.get('user_id') + '.png')
	}
});