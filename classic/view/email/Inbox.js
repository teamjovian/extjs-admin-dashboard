Ext.define('Admin.view.email.Inbox', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.inbox',
    // xtype: 'inbox',

	cls: 'email-inbox-panel shadow',
	bind: {
		store: '{inbox}'
	},
	viewConfig: {
		preserveScrollOnRefresh: !0,
		preserveScrollOnReload: !0
	},
	selModel: {
		selType: 'checkboxmodel',
		checkOnly: !0,
		showHeaderCheckbox: !0
	},
	listeners: {
		cellclick: 'onGridCellItemClick'
	},
	headerBorders: !1,
	rowLines: !1,
	scrollable: !1,
	columns: [{
        dataIndex: 'favorite',
        menuDisabled: !0,
        text: '<span class="x-fa fa-heart"></span>',
        width: 40,
        renderer: function (a) {
            return '<span class="x-fa fa-heart' + (a ? '' : '-o') + '"></span>'
        }
    }, {
        dataIndex: 'from',
        text: 'From',
        width: 140
    }, {
        dataIndex: 'title',
        text: 'Title',
        flex: 1
    }, {
        dataIndex: 'has_attachments',
        text: '<span class="x-fa fa-paperclip"></span>',
        width: 40,
        renderer: function (a) {
            return a ? '<span class="x-fa fa-paperclip"></span>' : ''
        }
    }, {
        xtype: 'datecolumn',
        dataIndex: 'received_on',
        width: 90,
        text: 'Received'
    }]
});
