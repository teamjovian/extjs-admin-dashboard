Ext.define('Admin.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    listen: {
        controller: {
            '#': {
                unmatchedroute: 'onRouteChange'
            }
        }
    },

    routes: {
        ':node': 'onRouteChange'
    },
    lastView: null,

	setCurrentView: function (b) {
		b = (b || '').toLowerCase();
		var h = this,
		k = h.getReferences(),
		f = k.mainCardPanel,
		e = f.getLayout(),
		i = k.navigationTreeList,
		j = i.getStore(),
		g = j.findNode('routeId', b) || j.findNode('viewType', b),
		l = g && g.get('viewType') || 'page404',
		d = h.lastView,
		c = f.child('component[routeId=' + b + ']'),
		a;
		if (d && d.isWindow) {
			d.destroy()
		}
		d = e.getActiveItem();
		if (!c) {
			a = Ext.create({
				xtype: l,
				routeId: b,
				hideMode: 'offsets'
			});
		}
		if (!a || !a.isWindow) {
			if (c) {
				if (c !== d) {
					e.setActiveItem(c)
				}
				a = c
			} else {
				Ext.suspendLayouts();
				e.setActiveItem(f.add(a));
				Ext.resumeLayouts(!0)
			}
		}
		i.setSelection(g);
		if (a.isFocusable(!0)) {
			a.focus()
		}
		h.lastView = a
	},

	onNavigationTreeSelectionChange: function (c, a) {
		var b = a && (a.get('routeId') || a.get('viewType'));
		if (b) {
			this.redirectTo(b)
		}
	},

	onToggleNavigationSize: function () {
		var f = this,
		e = f.getReferences(),
		a = e.navigationTreeList,
		b = e.mainContainerWrap,
		c = !a.getMicro(),
		d = c ? 64 : 250;
		if (Ext.isIE9m || !Ext.os.is.Desktop) {
			Ext.suspendLayouts();
			e.senchaLogo.setWidth(d);
			a.setWidth(d);
			a.setMicro(c);
			Ext.resumeLayouts();
			b.layout.animatePolicy = b.layout.animate = null;
			b.updateLayout()
		} else {
			if (!c) {
				a.setMicro(!1)
			}
			a.canMeasure = !1;
			e.senchaLogo.animate({
				dynamic: !0,
				to: {
					width: d
				}
			});
			a.width = d;
			b.updateLayout({
				isRoot: !0
			});
			a.el.addCls('nav-tree-animating');
			if (c) {
				a.on({
					afterlayoutanimation: function () {
						a.setMicro(!0);
						a.el.removeCls('nav-tree-animating');
						a.canMeasure = !0
					},
					single: !0
				})
			}
		}
	},

	onMainViewRender: function () {
		if (!window.location.hash) {
			this.redirectTo('dashboard')
		}
	},

	onRouteChange: function (a) {
		this.setCurrentView(a)
	},

	onSearchRouteChange: function () {
		this.setCurrentView('searchresults')
	},

	onSwitchToModern: function () {
		Ext.Msg.confirm('Switch to Modern', 'Are you sure you want to switch toolkits?', this.onSwitchToModernConfirmed, this)
	},

	onSwitchToModernConfirmed: function (b) {
		if (b === 'yes') {
			var a = window.location.search;
			a = a.replace(/(^\?|&)classic($|&)/, '').replace(/^\?/, '');
			window.location.search = ('?modern&' + a).replace(/&$/, '')
		}
	},

	onEmailRouteChange: function () {
		this.setCurrentView('email')
	}
});