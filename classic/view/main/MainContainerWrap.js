Ext.define('Admin.view.main.MainContainerWrap', {
    extend: 'Ext.container.Container',
    alias: 'widget.maincontainerwrap',
    // xtype: 'maincontainerwrap',

    scrollable: 'y',
    layout: {
        type: 'hbox',
        align: 'stretchmax',
        animate: !0,
        animatePolicy: {
            x: !0,
            width: !0
        }
    },
    
    beforeLayout: function () {
        var b = this,
        a = Ext.Element.getViewportHeight() - 64,
        c = b.getComponent('navigationTreeList');
        b.minHeight = a;
        c.setStyle({
            'min-height': a + 'px'
        });
        Ext.container.Container.prototype.beforeLayout.apply(this, arguments)
    }
});
