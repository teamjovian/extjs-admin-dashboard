Ext.define('Admin.view.forms.Wizards', {
    extend: 'Ext.container.Container',
    alias: 'widget.forms',
    // xtype: 'forms',

    requires: [
        'Admin.view.forms.WizardForm',
        'Admin.view.forms.WizardOne'
    ],

    cls: 'wizards',
    defaultFocus: 'wizardform',
    layout: 'responsivecolumn',
    items: [{
        xtype: 'formswizardone',
        userCls: 'big-100'
    }, {
        xtype: 'wizardform',
        cls: 'wizardtwo shadow',
        colorScheme: 'soft-purple',
        userCls: 'big-50 small-100'
    }, {
        xtype: 'wizardform',
        cls: 'wizardthree shadow',
        colorScheme: 'soft-green',
        userCls: 'big-50 small-100'
    }]
});