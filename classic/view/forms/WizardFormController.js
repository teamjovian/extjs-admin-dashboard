Ext.define('Admin.view.forms.WizardFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wizardform',

	init: function (c) {
		var d = this.lookupReference('navigation-toolbar'),
		b = d.items.items,
		a = c.colorScheme;
		if (a) {
			b[1].setUI(a);
			b[2].setUI(a)
		}
	},

	onNextClick: function (a) {
		var b = a.up('panel');
		b.getViewModel().set('atBeginning', !1);
		this.navigate(a, b, 'next')
	},

	onPreviousClick: function (a) {
		var b = a.up('panel');
		b.getViewModel().set('atEnd', !1);
		this.navigate(a, b, 'prev')
	},

	navigate: function (k, e, i) {
		var g = e.getLayout(),
		j = this.lookupReference('progress'),
		h = e.getViewModel(),
		f = j.items.items,
		a,
		c,
		d,
		b;
		g[i]();
		d = g.getActiveItem();
		b = e.items.indexOf(d);
		for (c = 0; c < f.length; c++) {
			a = f[c];
			if (b === a.step) {
				a.setPressed(!0)
			} else {
				a.setPressed(!1)
			}
			if (Ext.isIE8) {
				a.btnIconEl.syncRepaint()
			}
		}
		d.focus();
		if (b === 0) {
			h.set('atBeginning', !0)
		}
		if (b === 3) {
			h.set('atEnd', !0)
		}
	}
});