Ext.define('Admin.view.search.ResultsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.searchresults',

    requires: [
        'Shared.store.search.Results',
        'Shared.store.search.Users',
        'Shared.store.email.Inbox'
    ],

    stores: {
        allResults: {
            type: 'searchresults'
        },
        usersResults: {
            type: 'searchusers'
        },
        inboxResults: {
            type: 'inbox'
        }
    }
});