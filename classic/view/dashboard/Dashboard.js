Ext.define('Admin.view.dashboard.Dashboard', {
    extend: 'Ext.container.Container',
    alias: 'widget.admindashboard',
    // xtype: 'admindashboard',

    requires: [
        'Shared.view.dashboard.DashboardController',
        'Shared.view.dashboard.DashboardModel',

        'Admin.view.dashboard.Earnings',
        'Admin.view.dashboard.HDDUsage',
        'Admin.view.dashboard.Network',
        'Admin.view.dashboard.Sales',
        'Admin.view.dashboard.Services',
        'Admin.view.dashboard.Todos',
        'Admin.view.dashboard.TopMovies',
        'Shared.view.dashboard.Weather'
    ],

    controller: 'dashboard',
    viewModel: {
        type: 'dashboard'
    },
    layout: 'responsivecolumn',
    listeners: {
        hide: 'onHideView'
    },
    items: [{
        xtype: 'network',
        userCls: 'big-60 small-100'
    }, {
        xtype: 'hddusage',
        userCls: 'big-20 small-50'
    }, {
        xtype: 'earnings',
        userCls: 'big-20 small-50'
    }, {
        xtype: 'sales',
        userCls: 'big-20 small-50'
    }, {
        xtype: 'topmovies',
        userCls: 'big-20 small-50'
    }, {
        xtype: 'weather',
        cls: 'weather-panel shadow',
        userCls: 'big-40 small-100'
    }, {
        xtype: 'todo',
        userCls: 'big-60 small-100'
    }, {
        xtype: 'services',
        userCls: 'big-40 small-100'
    }]
});