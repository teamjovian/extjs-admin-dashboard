Ext.define('Admin.view.dashboard.Sales', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.sales',
    // xtype: 'sales',

    ui: 'light',
    iconCls: 'x-fa fa-briefcase',
    headerPosition: 'bottom',
    cls: 'quick-graph-panel shadow',
    height: 130,
    layout: 'fit',
    title: 'Sales',
    items: [{
        xtype: 'cartesian',
        animation: !Ext.isIE9m && Ext.os.is.Desktop,
        downloadServerUrl: Shared.Config.downloadServerUrl,
        height: 75,
        background: '#8561c5',
        colors: ['#ffffff'],
        bind: '{quarterlyGrowth}',
        axes: [{
            type: 'category',
            fields: ['xvalue'],
            hidden: !0,
            position: 'bottom'
        }, {
            type: 'numeric',
            fields: ['yvalue'],
            grid: {
                odd: {
                    fill: '#e8e8e8'
                }
            },
            hidden: !0,
            position: 'left'
        }],
        series: [{
            type: 'bar',
            xField: 'xvalue',
            yField: ['yvalue']
        }],
        interactions: [{
            type: 'panzoom'
        }]
    }]
});