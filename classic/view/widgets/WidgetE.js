Ext.define('Admin.view.widgets.WidgetE', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.widget-e',
    // xtype: 'widget-e',

    cls: 'admin-widget-small sale-panel info-card-item shadow',
    containerColor: '',
    height: 170,
    data: {
        amount: 0,
        type: '',
        icon: ''
    },
    tpl: '<div><h2>{amount}</h2><div>{type}</div><span class="x-fa fa-{icon}"></span></div>',
    
    initComponent: function () {
        var a = this;
        Ext.apply(a, {
            cls: a.config.containerColor
        });
        Ext.panel.Panel.prototype.initComponent.apply(this, arguments)
    }
});