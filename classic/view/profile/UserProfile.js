Ext.define('Admin.view.profile.UserProfile', {
    extend: 'Shared.view.profile.UserProfileBase',
    alias: 'widget.profile',
    // xtype: 'profile',

    requires: [
        'Shared.view.profile.UserProfileModel', 

        'Shared.view.profile.Description', 
        'Shared.view.profile.Notifications', 
        'Admin.view.profile.ShareUpdate', 
        'Shared.view.profile.Social', 
        'Shared.view.profile.Timeline'
    ],

    cls: 'userProfile-container',
    layout: 'responsivecolumn',
    items: [{
        xtype: 'profileshare',
        userCls: 'big-100 small-100 shadow'
    }, {
        xtype: 'profilesocial',
        userCls: 'big-50 small-100 shadow'
    }, {
        xtype: 'profiledescription',
        userCls: 'big-50 small-100 shadow'
    }, {
        xtype: 'profilenotifications',
        userCls: 'big-50 small-100 shadow'
    }, {
        xtype: 'profiletimeline',
        userCls: 'big-50 small-100 shadow'
    }]
});