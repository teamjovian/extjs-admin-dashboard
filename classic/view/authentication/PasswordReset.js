Ext.define('Admin.view.authentication.PasswordReset', {
    extend: 'Admin.view.authentication.LockingWindow',
    alias: 'widget.passwordreset',
    // xtype: 'passwordreset',

    title: 'Reset Password',
    defaultFocus: 'authdialog',
    items: [{
        xtype: 'authdialog',
        width: 455,
        defaultButton: 'resetPassword',
        autoComplete: !0,
        bodyPadding: '20 20',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        defaults: {
            margin: '10 0'
        },
        cls: 'auth-dialog-login',
        items: [{
            xtype: 'label',
            cls: 'lock-screen-top-label',
            text: 'Enter your email address for further reset instructions'
        }, {
            xtype: 'textfield',
            cls: 'auth-textbox',
            height: 55,
            name: 'email',
            hideLabel: !0,
            allowBlank: !1,
            emptyText: 'user@example.com',
            vtype: 'email',
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-email-trigger'
                }
            }
        }, {
            xtype: 'button',
            reference: 'resetPassword',
            scale: 'large',
            ui: 'soft-blue',
            formBind: !0,
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: 'Reset Password',
            listeners: {
                click: 'onResetClick'
            }
        }, {
            xtype: 'component',
            html: '<div style="text-align:right"><a href="#login" class="link-forgot-password">Back to Log In</a></div>'
        }]
    }]
});