Ext.define('Admin.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',
	
	onFaceBookLogin: function () {
		this.redirectTo('dashboard', !0)
	},

	onLoginButton: function () {
		this.redirectTo('dashboard', !0)
	},

	onLoginAsButton: function () {
		this.redirectTo('login', !0)
	},

	onNewAccount: function () {
		this.redirectTo('register', !0)
	},

	onSignupClick: function () {
		this.redirectTo('dashboard', !0)
	},
    
	onResetClick: function () {
		this.redirectTo('dashboard', !0)
	}
});