Ext.define('Admin.view.authentication.AuthenticationModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.authentication',

	data: {
		userid: '',
		fullName: '',
		password: '',
		email: '',
		persist: !1,
		agrees: !1
	}
});