Ext.define('Admin.view.authentication.LockingWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.lockingwindow',
    // xtype: 'lockingwindow',

	requires: [
		'Admin.view.authentication.Dialog'
	],

	cls: 'auth-locked-window',
	closable: !1,
	resizable: !1,
	autoShow: !0,
	titleAlign: 'center',
	maximized: !0,
	modal: !0,
	layout: {
		type: 'vbox',
		align: 'center',
		pack: 'center'
	},
	controller: 'authentication'
});