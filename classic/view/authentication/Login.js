Ext.define('Admin.view.authentication.Login', {
    extend: 'Admin.view.authentication.LockingWindow',
    alias: 'widget.login',
    // xtype: 'login',

    title: "Let's Log In",
    defaultFocus: 'authdialog',
    items: [{
        xtype: 'authdialog',
        defaultButton: 'loginButton',
        autoComplete: !0,
        bodyPadding: '20 20',
        cls: 'auth-dialog-login',
        header: !1,
        width: 415,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        defaults: {
            margin: '5 0'
        },
        items: [{
            xtype: 'label',
            text: 'Sign into your account'
        }, {
            xtype: 'textfield',
            cls: 'auth-textbox',
            name: 'userid',
            bind: '{userid}',
            height: 55,
            hideLabel: !0,
            allowBlank: !1,
            emptyText: 'user id',
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-email-trigger'
                }
            }
        }, {
            xtype: 'textfield',
            cls: 'auth-textbox',
            height: 55,
            hideLabel: !0,
            emptyText: 'Password',
            inputType: 'password',
            name: 'password',
            bind: '{password}',
            allowBlank: !1,
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-password-trigger'
                }
            }
        }, {
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'checkboxfield',
                flex: 1,
                cls: 'form-panel-font-color rememberMeCheckbox',
                height: 30,
                bind: '{persist}',
                boxLabel: 'Remember me'
            }, {
                xtype: 'box',
                html: '<a href="#passwordreset" class="link-forgot-password"> Forgot Password ?</a>'
            }]
        }, {
            xtype: 'button',
            reference: 'loginButton',
            scale: 'large',
            ui: 'soft-green',
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: 'Login',
            formBind: !0,
            listeners: {
                click: 'onLoginButton'
            }
        }, {
            xtype: 'box',
            html: '<div class="outer-div"><div class="seperator">OR</div></div>',
            margin: '10 0'
        }, {
            xtype: 'button',
            scale: 'large',
            ui: 'facebook',
            iconAlign: 'right',
            iconCls: 'x-fa fa-facebook',
            text: 'Login with Facebook',
            listeners: {
                click: 'onFaceBookLogin'
            }
        }, {
            xtype: 'box',
            html: '<div class="outer-div"><div class="seperator">OR</div></div>',
            margin: '10 0'
        }, {
            xtype: 'button',
            scale: 'large',
            ui: 'gray',
            iconAlign: 'right',
            iconCls: 'x-fa fa-user-plus',
            text: 'Create Account',
            listeners: {
                click: 'onNewAccount'
            }
        }]
    }],

    initComponent: function () {
        this.addCls('user-login-register-container');
        Admin.view.authentication.LockingWindow.prototype.initComponent.apply(this, arguments)
    }
});