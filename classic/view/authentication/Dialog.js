Ext.define('Admin.view.authentication.Dialog', {
    extend: 'Ext.form.Panel',
    alias: 'widget.authdialog',
    // xtype: 'authdialog',

    requires: [
        'Admin.view.authentication.AuthenticationController',
        'Admin.view.authentication.AuthenticationModel',
    ],

	controller: 'authentication',
	viewModel: {
		type: 'authentication'
	},
	defaultFocus: 'textfield:focusable:not([hidden]):not([disabled]):not([value])',
	autoComplete: !1,

	initComponent: function () {
		var a = this,
		b;
		if (a.autoComplete) {
			a.autoEl = Ext.applyIf(a.autoEl || {}, {
					tag: 'form',
					name: 'authdialog',
					method: 'post'
				})
		}
		a.addCls('auth-dialog');
		Ext.form.Panel.prototype.initComponent.call(this);
		if (a.autoComplete) {
			b = {
				afterrender: 'doAutoComplete',
				scope: a,
				single: !0
			};
			Ext.each(a.query('textfield'), function (a) {
				a.on(b)
			})
		}
	},

	doAutoComplete: function (a) {
		if (a.inputEl && a.autoComplete !== !1) {
			a.inputEl.set({
				autocomplete: 'on'
			})
		}
	}
});
