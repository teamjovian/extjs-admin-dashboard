Ext.define('Admin.view.authentication.Register', {
    extend: 'Admin.view.authentication.LockingWindow',
    alias: 'widget.register',
    // xtype: 'register',

    title: 'User Registration',
    defaultFocus: 'authdialog',
    items: [{
        xtype: 'authdialog',
        bodyPadding: '20 20',
        width: 455,
        reference: 'authDialog',
        defaultButton: 'submitButton',
        autoComplete: !0,
        cls: 'auth-dialog-register',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        defaults: {
            margin: '10 0',
            selectOnFocus: !0
        },
        items: [{
            xtype: 'label',
            cls: 'lock-screen-top-label',
            text: 'Create an account'
        }, {
            xtype: 'textfield',
            cls: 'auth-textbox',
            height: 55,
            hideLabel: !0,
            allowBlank: !1,
            emptyText: 'Fullname',
            name: 'fullName',
            bind: '{fullName}',
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-email-trigger'
                }
            }
        }, {
            xtype: 'textfield',
            cls: 'auth-textbox',
            height: 55,
            hideLabel: !0,
            allowBlank: !1,
            name: 'userid',
            bind: '{userid}',
            emptyText: 'Username',
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-email-trigger'
                }
            }
        }, {
            xtype: 'textfield',
            cls: 'auth-textbox',
            height: 55,
            hideLabel: !0,
            allowBlank: !1,
            name: 'email',
            emptyText: 'user@example.com',
            vtype: 'email',
            bind: '{email}',
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-envelope-trigger'
                }
            }
        }, {
            xtype: 'textfield',
            cls: 'auth-textbox',
            height: 55,
            hideLabel: !0,
            allowBlank: !1,
            emptyText: 'Password',
            name: 'password',
            inputType: 'password',
            bind: '{password}',
            triggers: {
                glyphed: {
                    cls: 'trigger-glyph-noop auth-password-trigger'
                }
            }
        }, {
            xtype: 'checkbox',
            flex: 1,
            name: 'agrees',
            cls: 'form-panel-font-color rememberMeCheckbox',
            height: 32,
            bind: '{agrees}',
            allowBlank: !1,
            boxLabel: 'I agree with the Terms and Conditions',
            isValid: function () {
                var a = this;
                return a.checked || a.disabled
            }
        }, {
            xtype: 'button',
            scale: 'large',
            ui: 'soft-blue',
            formBind: !0,
            reference: 'submitButton',
            bind: !1,
            margin: '5 0',
            iconAlign: 'right',
            iconCls: 'x-fa fa-angle-right',
            text: 'Signup',
            listeners: {
                click: 'onSignupClick'
            }
        }, {
            xtype: 'box',
            html: '<div class="outer-div"><div class="seperator">OR</div></div>'
        }, {
            xtype: 'button',
            scale: 'large',
            ui: 'facebook',
            margin: '5 0',
            iconAlign: 'right',
            iconCls: 'x-fa fa-facebook',
            text: 'Login with Facebook',
            listeners: {
                click: 'onFaceBookLogin'
            }
        }, {
            xtype: 'component',
            html: '<div style="text-align:right"><a href="#login" class="link-forgot-password">Back to Log In</a></div>'
        }]
    }]
});