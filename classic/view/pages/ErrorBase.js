Ext.define('Admin.view.pages.ErrorBase', {
    extend: 'Ext.window.Window',
    
	requires: [
		'Admin.view.authentication.AuthenticationController'
	],

	controller: 'authentication',
	autoShow: !0,
	cls: 'error-page-container',
	closable: !1,
	title: 'Sencha',
	titleAlign: 'center',
	maximized: !0,
	modal: !0,
	layout: {
		type: 'vbox',
		align: 'center',
		pack: 'center'
	}
});