Ext.define('Admin.view.pages.Error500Window', {
    extend: 'Admin.view.pages.ErrorBase',
    alias: 'widget.page500',
    // xtype: 'page500',

    items: [{
        xtype: 'container',
        width: 600,
        cls: 'error-page-inner-container',
        layout: {
            type: 'vbox',
            align: 'center',
            pack: 'center'
        },
        items: [{
            xtype: 'label',
            cls: 'error-page-top-text',
            text: '500'
        }, {
            xtype: 'label',
            cls: 'error-page-desc',
            html: '<div>Something went wrong and server could not process your request.</div><div>Try going back to our <a href="#dashboard"> Home page </a></div>'
        }, {
            xtype: 'tbspacer',
            flex: 1
        }]
    }]
});