Ext.define('Admin.view.charts.Charts', {
    extend: 'Ext.container.Container',
    alias: 'widget.charts',
    // xtype: 'charts',

    requires: [
        'Shared.view.charts.ChartsModel',

        'Shared.view.charts.Area',
        'Shared.view.charts.Bar',
        'Shared.view.charts.Bounces',
        'Shared.view.charts.Gauge',
        'Shared.view.charts.Line',
        'Shared.view.charts.Network',
        'Shared.view.charts.Pie',
        'Shared.view.charts.Pie3D',
        'Shared.view.charts.Polar',
        'Shared.view.charts.Stacked',
        'Shared.view.charts.Visitors'
    ],

    viewModel: {
        type: 'charts'
    },
    layout: 'responsivecolumn',
    defaults: {
        defaults: {
            animation: !Ext.isIE9m && Ext.os.is.Desktop
        }
    },
    items: [{
        xtype: 'chartsareapanel',
        userCls: 'big-50 small-100'
    }, {
        xtype: 'chartspie3dpanel',
        userCls: 'big-50 small-100'
    }, {
        xtype: 'chartspolarpanel',
        userCls: 'big-50 small-100'
    }, {
        xtype: 'chartsstackedpanel',
        userCls: 'big-50 small-100'
    }, {
        xtype: 'chartsbarpanel',
        userCls: 'big-50 small-100'
    }, {
        xtype: 'chartsgaugepanel',
        userCls: 'big-50 small-100'
    }]
});