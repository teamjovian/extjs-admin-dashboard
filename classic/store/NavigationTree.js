Ext.define('Admin.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    requires: [
        'Admin.view.dashboard.Dashboard',
        'Admin.view.profile.UserProfile',
        'Admin.view.search.Results',
        'Admin.view.pages.FAQ',
        'Admin.view.widgets.Widgets',
        'Admin.view.forms.Wizards',
        'Admin.view.charts.Charts',
        'Admin.view.email.Email',

        'Admin.view.pages.BlankPage',
        'Admin.view.pages.Error404Window',
        'Admin.view.pages.Error500Window',

        'Admin.view.authentication.LockScreen',
        'Admin.view.authentication.Login',
        'Admin.view.authentication.PasswordReset',
        'Admin.view.authentication.Register'
    ],
    
    storeId: 'NavigationTree',
    fields: [{
        name: 'text'
    }],
    root: {
        expanded: !0,
        children: [{
            text: 'Dashboard',
            iconCls: 'x-fa fa-desktop',
            rowCls: 'nav-tree-badge nav-tree-badge-new',
            viewType: 'admindashboard',
            routeId: 'dashboard',
            leaf: !0
        }, {
            text: 'Email',
            iconCls: 'x-fa fa-send',
            rowCls: 'nav-tree-badge nav-tree-badge-hot',
            viewType: 'email',
            leaf: !0
        }, {
            text: 'Profile',
            iconCls: 'x-fa fa-user',
            viewType: 'profile',
            leaf: !0
        }, {
            text: 'Search results',
            iconCls: 'x-fa fa-search',
            viewType: 'searchresults',
            leaf: !0
        }, {
            text: 'FAQ',
            iconCls: 'x-fa fa-question',
            viewType: 'faq',
            leaf: !0
        }, {
            text: 'Pages',
            iconCls: 'x-fa fa-leanpub',
            expanded: !1,
            selectable: !1,
            children: [{
                text: 'Blank Page',
                iconCls: 'x-fa fa-file-o',
                viewType: 'pageblank',
                leaf: !0
            }, {
                text: '404 Error',
                iconCls: 'x-fa fa-exclamation-triangle',
                viewType: 'page404',
                leaf: !0
            }, {
                text: '500 Error',
                iconCls: 'x-fa fa-times-circle',
                viewType: 'page500',
                leaf: !0
            }, {
                text: 'Lock Screen',
                iconCls: 'x-fa fa-lock',
                viewType: 'lockscreen',
                leaf: !0
            }, {
                text: 'Login',
                iconCls: 'x-fa fa-check',
                viewType: 'login',
                leaf: !0
            }, {
                text: 'Register',
                iconCls: 'x-fa fa-pencil-square-o',
                viewType: 'register',
                leaf: !0
            }, {
                text: 'Password Reset',
                iconCls: 'x-fa fa-lightbulb-o',
                viewType: 'passwordreset',
                leaf: !0
            }]
        }, {
            text: 'Widgets',
            iconCls: 'x-fa fa-flask',
            viewType: 'widgets',
            leaf: !0
        }, {
            text: 'Forms',
            iconCls: 'x-fa fa-edit',
            viewType: 'forms',
            leaf: !0
        }, {
            text: 'Charts',
            iconCls: 'x-fa fa-pie-chart',
            viewType: 'charts',
            leaf: !0
        }]
    }
});