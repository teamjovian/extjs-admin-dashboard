Ext.define('Admin.view.main.Main', {
    extend: 'Ext.Container',
    requires: [
        'Admin.view.main.MainController',
        'Admin.view.main.Toolbar'
    ],

    controller: 'main',
    platformConfig: {
        phone: {
            controller: 'phone-main'
        }
    },
    layout: 'hbox',
    items: [{
        xtype: 'maintoolbar',
        docked: 'top',
        userCls: 'main-toolbar shadow'
    }, {
        xtype: 'container',
        userCls: 'main-nav-container',
        reference: 'navigation',
        layout: 'fit',
        items: [{
            xtype: 'treelist',
            reference: 'navigationTree',
            scrollable: !0,
            ui: 'nav',
            store: 'NavigationTree',
            expanderFirst: !1,
            expanderOnly: !1,
            listeners: {
                itemclick: 'onNavigationItemClick',
                selectionchange: 'onNavigationTreeSelectionChange'
            }
        }]
    }, {
        xtype: 'navigationview',
        flex: 1,
        reference: 'mainCard',
        userCls: 'main-container',
        navigationBar: !1
    }]
});