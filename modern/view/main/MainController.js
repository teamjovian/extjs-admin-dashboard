Ext.define('Admin.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    listen: {
        controller: {
            '#': {
                unmatchedroute: 'onRouteChange'
            }
        }
    },

    routes: {
        ':node': 'onRouteChange'
    },

    config: {
        showNavigation: !0
    },

    collapsedCls: 'main-nav-collapsed',

    init: function (c) {
        var a = this,
        b = a.getReferences();
        Ext.app.ViewController.prototype.init.call(this, c);
        a.nav = b.navigation;
        a.navigationTree = b.navigationTree
    },

    onNavigationItemClick: function () {},

    onNavigationTreeSelectionChange: function (c, a) {
        var b = a && (a.get('routeId') || a.get('viewType'));
        if (b) {
            this.redirectTo(b)
        }
    },

    onRouteChange: function (a) {
        this.setCurrentView(a)
    },

    onSwitchToClassic: function () {
        Ext.Msg.confirm('Switch to Classic', 'Are you sure you want to switch toolkits?', this.onSwitchToClassicConfirmed, this)
    },

    onSwitchToClassicConfirmed: function (b) {
        if (b === 'yes') {
            var a = location.search;
            a = a.replace(/(^\?|&)modern($|&)/, '').replace(/^\?/, '');
            location.search = ('?classic&' + a).replace(/&$/, '')
        }
    },

    onToggleNavigationSize: function () {
        this.setShowNavigation(!this.getShowNavigation())
    },

    setCurrentView: function (a) {
        a = (a || '').toLowerCase();
        var g = this,
        h = g.getReferences(),
        b = h.mainCard,
        d = g.navigationTree,
        e = d.getStore(),
        f = e.findNode('routeId', a) || e.findNode('viewType', a),
        c = b.child('component[routeId=' + a + ']');
        if (!c) {
            c = b.add({
                    xtype: f.get('viewType'),
                    routeId: a
                })
        }
        b.setActiveItem(c);
        d.setSelection(f)
    },

    updateShowNavigation: function (d, i) {
        if (i !== undefined) {
            var c = this,
            g = c.collapsedCls,
            f = c.getReferences(),
            e = f.logo,
            h = c.nav,
            a = f.navigationTree,
            b = a.rootItem.el;
            h.toggleCls(g);
            e.toggleCls(g);
            if (d) {
                a.setMicro(!1)
            } else {
                b.setWidth(b.getWidth())
            }
            e.element.on({
                transitionend: function () {
                    if (d) {
                        b.setWidth('')
                    } else {
                        a.setMicro(!0)
                    }
                },
                single: !0
            })
        }
    },

    toolbarButtonClick: function (b) {
        var a = b.config.href;
        this.redirectTo(a)
    }
});