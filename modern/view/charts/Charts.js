Ext.define('Admin.view.charts.Charts', {
    extend: 'Ext.Container',
    alias: 'widget.charts',
    // xtype: 'charts',

    requires: [
        'Shared.view.charts.ChartsModel',

        'Shared.view.charts.Area',
        'Shared.view.charts.Bar',
        'Shared.view.charts.Bounces',
        'Shared.view.charts.Gauge',
        'Shared.view.charts.Line',
        'Shared.view.charts.Network',
        'Shared.view.charts.Pie',
        'Shared.view.charts.Pie3D',
        'Shared.view.charts.Polar',
        'Shared.view.charts.Stacked',
        'Shared.view.charts.Visitors'
    ],

    cls: 'dashboard',
    scrollable: !0,

    viewModel: {
        type: 'charts'
    },
    
    items: [{
        xtype: 'chartsareapanel',
        userCls: 'big-50 small-100 dashboard-item shadow'
    }, {
        xtype: 'chartspie3dpanel',
        userCls: 'big-50 small-100 dashboard-item shadow'
    }, {
        xtype: 'chartspolarpanel',
        userCls: 'big-50 small-100 dashboard-item shadow'
    }, {
        xtype: 'chartsstackedpanel',
        userCls: 'big-50 small-100 dashboard-item shadow'
    }, {
        xtype: 'chartsbarpanel',
        userCls: 'big-50 small-100 dashboard-item shadow'
    }, {
        xtype: 'chartsgaugepanel',
        userCls: 'big-50 small-100 dashboard-item shadow'
    }]
});