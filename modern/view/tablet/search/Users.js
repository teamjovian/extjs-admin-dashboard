Ext.define('Admin.view.tablet.search.Users', {
    extend: 'Ext.grid.Grid',

    columns: [{
        text: '#',
        width: 40,
        dataIndex: 'identifier'
    }, {
        text: 'User',
        sortable: !1,
        cell: {
            encodeHtml: !1
        },
        renderer: function (a) {
            return "<img src='resources/images/user-profile/" + a + "' alt='Profile Pic' height='40px' width='40px' class='circular'>"
        },
        width: 75,
        dataIndex: 'profile_pic'
    }, {
        text: 'Name',
        cls: 'content-column',
        flex: 1,
        dataIndex: 'fullname'
    }, {
        text: 'Email',
        cls: 'content-column',
        dataIndex: 'email',
        flex: 1
    }, {
        xtype: 'datecolumn',
        text: 'Date',
        cls: 'content-column',
        width: 120,
        dataIndex: 'joinDate'
    }, {
        text: 'Subscription',
        cls: 'content-column',
        dataIndex: 'subscription',
        flex: 1
    }]
});