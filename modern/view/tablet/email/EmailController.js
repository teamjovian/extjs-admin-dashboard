Ext.define('Admin.view.tablet.email.EmailController', {
    extend: 'Admin.view.email.EmailController',
    alias: 'controller.email-tablet',

    closeComposer: function () {
        var a = this.composer;
        if (a) {
            this.composer = null;
            a.destroy()
        }
    },

    doCompose: function (d) {
        var a = this,
        b = a.composer,
        e = a.getView(),
        c;
        a.hideActions();
        if (!b) {
            a.composer = b = Ext.Viewport.add({
                    xtype: 'compose',
                    floated: !0,
                    modal: !0,
                    centered: !0,
                    ownerCt: e,
                    width: '80%',
                    height: '80%'
                });
            if (d) {
                c = a.lookupReference('toField');
                c.setValue(d)
            }
        }
        b.show()
    },

    onChangeFilter: function () {
        this.hideActions();
        Admin.view.email.EmailController.prototype.onChangeFilter.apply(this, arguments)
    },

    onSwipe: function (a) {
        if (this.lookup('controls').isHidden() && a.direction === 'left') {
            this.showActions()
        }
    },

    onCloseMessage: function () {
        this.closeComposer()
    },
    
    onSendMessage: function () {
        this.closeComposer()
    }
});