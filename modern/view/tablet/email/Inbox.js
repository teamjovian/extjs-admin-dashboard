Ext.define('Admin.view.tablet.email.Inbox', {
    extend: 'Ext.grid.Grid',

    itemConfig: {
        viewModel: !0,
        bind: {
            userCls: 'inbox-{record.read:pick("unread","read")}'
        }
    },
    plugins: [{
        type: 'multiselection',
        selectionColumn: {
            hidden: !1,
            width: 40
        }
    }],
    rowLines: !1,
    striped: !1,
    columns: [{
        text: '<span class="x-fa fa-heart"></span>',
        menuDisabled: !0,
        width: 36,
        dataIndex: 'favorite',
        userCls: 'inbox-favorite-icon',
        align: 'center',
        cell: {
            align: 'center',
            bind: {
                bodyCls: 'x-fa {record.favorite:pick("fa-heart-o", "fa-heart inbox-favorite-icon")}'
            }
        },
        renderer: function () {
            return ''
        }
    }, {
        text: 'From',
        dataIndex: 'from',
        width: 150,
        cell: {
            bodyCls: 'inbox-from'
        }
    }, {
        text: 'Title',
        dataIndex: 'title',
        flex: 1,
        cell: {
            bodyCls: 'inbox-title'
        }
    }, {
        text: '<span class="x-fa fa-paperclip"></span>',
        width: 40,
        align: 'center',
        dataIndex: 'has_attachments',
        cell: {
            align: 'center',
            bind: {
                bodyCls: 'x-fa {record.has_attachments:pick("", "fa-paperclip")}'
            }
        },
        renderer: function () {
            return ''
        }
    }, {
        text: 'Received',
        xtype: 'datecolumn',
        format: 'Y-m-d',
        dataIndex: 'received_on',
        width: 90
    }]
});