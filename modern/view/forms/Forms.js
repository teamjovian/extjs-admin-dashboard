Ext.define('Admin.view.forms.Forms', {
    extend: 'Ext.Container',
    alias: 'widget.forms',
    // xtype: 'forms',

    requires: [
        'Admin.view.forms.Account',
        'Admin.view.forms.Address',
        'Admin.view.forms.Finish',
        'Admin.view.forms.Profile',
        'Admin.view.forms.SpecialOffer',
        'Admin.view.forms.Wizard'
    ],

    cls: 'dashboard',
    scrollable: !0,
    defaults: {
        height: '25em'
    },
    items: [{
        xtype: 'wizard',
        back: { ui: 'wizard' },
        next: { ui: 'wizard' },
        tabBar: {
            ui: 'wizard',
            defaultTabUI: 'wizard',
            layout: { pack: 'center' }
        },
        userCls: 'big-100 small-100 dashboard-item shadow',
        plugins: 'responsive',
        responsiveConfig: {
            'phone && width < 1000': { height: '25em' },
            '!phone && width < 1000': { height: '50em' },
            'width >= 1000': { height: '25em' }
        },
        defaults: {tab: { iconAlign: 'top' }},
        items: [{
            xtype: 'specialoffer',
            plugins: 'responsive',
            platformConfig: {
                phone: {
                    hidden: !0
                }
            },
            responsiveConfig: {
                'width < 1000': {
                    docked: 'top',
                    width: null,
                    height: '25em'
                },
                'width >= 1000': {
                    docked: 'left',
                    width: '50%',
                    height: null
                }
            }
        }, {
            xtype: 'accountform'
        }, {
            xtype: 'profileform'
        }, {
            xtype: 'addressform'
        }, {
            xtype: 'finishform'
        }]
    }]
});