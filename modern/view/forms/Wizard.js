Ext.define('Admin.view.forms.Wizard', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.wizard',
    // xtype: 'wizard',

    isWizard: !0,
    config: {
        appendButtons: !0,
        controlBar: {
            xtype: 'toolbar',
            docked: 'bottom',
            border: !1,
            items: ['->']
        },
        next: {
            xtype: 'button',
            ui: 'action',
            text: 'Next',
            margin: '0 8 0 8',
            itemId: 'next'
        },
        back: {
            xtype: 'button',
            ui: 'action',
            text: 'Back',
            margin: '0 0 0 8',
            itemId: 'back'
        }
    },
    cls: 'wizard',
    initialize: function () {
        var a = this,
        c,
        d,
        e,
        b;
        Ext.tab.Panel.prototype.initialize.call(this);
        b = Ext.clone(a.getControlBar());
        e = b.items || (b.items = []);
        d = a.getNext();
        c = a.getBack();
        if (a.getAppendButtons()) {
            e.push(c, d)
        } else {
            e.unshift(c, d)
        }
        b = a.add(b);
        a.nextBtn = d = b.getComponent('next');
        a.backBtn = c = b.getComponent('back');
        d.on({
            tap: a.onNext,
            scope: a
        });
        c.on({
            tap: a.onBack,
            scope: a
        });
        this.syncBackNext()
    },
    
    onBack: function () {
        var a = this.getItemIndex();
        if (a > 0) {
            this.setActiveItem(a - 1)
        }
    },

    onNext: function () {
        var a = this,
        b = a.getItemIndex(),
        c = a.getTabBar().getItems().length;
        if (b < c - 1) {
            a.allowNext = !0;
            a.setActiveItem(b + 1);
            a.allowNext = !1
        }
    },

    getItemIndex: function (a) {
        var b = this.getTabBar(),
        c = b.getItems();
        a = a || this.getActiveItem();
        return c.indexOf(a.tab)
    },

    syncBackNext: function () {
        var c = this,
        g = c.nextBtn,
        f = c.backBtn,
        d,
        a,
        b,
        h,
        e;
        if (g && f) {
            a = c.getItemIndex();
            e = c.getTabBar();
            b = e.getItems();
            f.setDisabled(!a);
            g.setDisabled(a >= b.length - 1);
            for (d = b.length; d-- > 0; ) {
                h = b.getAt(d);
                h.setDisabled(d > a)
            }
        }
    },

    updateActiveItem: function () {
        Ext.tab.Panel.prototype.updateActiveItem.apply(this, arguments);
        this.syncBackNext()
    }
});