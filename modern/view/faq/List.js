Ext.define('Admin.view.faq.List', {
    extend: 'Ext.dataview.List',
    alias: 'widget.faq',
    // xtype: 'faq',

    requires: [
        'Admin.view.faq.Items',
        'Shared.store.faq.FAQ',
    ],

    container: {
        userCls: 'dashboard'
    },
    
    store: {
        type: 'faq',
        autoLoad: !0
    },

    itemConfig: {
        xtype: 'faqitems',
        ui: 'light',
        userCls: 'dashboard-item shadow',
        viewModel: !0,
        bind: {
            title: '{record.name}',
            store: '{record.questions}'
        }
    }
});