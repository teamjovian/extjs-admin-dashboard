Ext.define('Admin.view.faq.Items', {
    extend: 'Ext.Panel',
    alias: 'widget.faqitems',
    // xtype: 'faqitems',

    requires: [
        'Admin.view.faq.ItemsController'
    ],

    controller: 'faqitems',
    bodyPadding: '0 20 20 20',
    config: {
        store: null
    },
    
    items: [{
        xtype: 'dataview',
        scrollable: !1,
        listeners: {
            childtap: 'onChildTap'
        },
        itemTpl: '<div class="faq-item"><div class="faq-title"><div class="faq-expander x-fa"></div><div class="faq-question">{question}</div></div><div class="faq-body"><div>{answer}</div></div></div>'
    }],

    updateStore: function (a) {
        var b = this.down('dataview');
        b.setStore(a)
    }
});