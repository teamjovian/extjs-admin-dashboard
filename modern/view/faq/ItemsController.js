Ext.define('Admin.view.faq.ItemsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.faqitems',
    // xtype: 'faqitems',

    animateBody: function (a, b, c) {
        var d = this.getView();
        a.animate({
            duration: 200,
            from: {
                height: b
            },
            to: {
                height: c
            }
        })
    },
    
    collapseBody: function (b) {
        var a = b.down('.faq-body'),
        c = a.getHeight();
        b.removeCls('faq-expanded');
        this.animateBody(a, c, 0)
    },

    expandBody: function (c) {
        var a = c.down('.faq-body'),
        b;
        a.setStyle('height', 'auto');
        b = a.getHeight();
        c.addCls('faq-expanded');
        this.animateBody(a, 0, b)
    },

    onChildTap: function (g, d) {
        var c = this,
        e = d.event.target,
        b = d.child,
        f = e && Ext.fly(e).getStyle('cursor'),
        a;
        if (f === 'pointer') {
            if (b.hasCls('faq-expanded')) {
                c.collapseBody(b)
            } else {
                a = g.element.down('.faq-expanded');
                if (a) {
                    c.collapseBody(a)
                }
                c.expandBody(b)
            }
        }
    }
});