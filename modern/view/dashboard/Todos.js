Ext.define('Admin.view.dashboard.Todos', {
    extend: 'Ext.Panel',
    alias: 'widget.todo',
    // xtype: 'todo',

    cls: 'todo-list shadow',
    title: 'TODO List',
    bodyPadding: 15,
    layout: 'vbox',
    items: [{
        xtype: 'grid',
        flex: 1,
        userCls: 'dashboard-todo-list',
        hideHeaders: !0,
        bind: {
            store: '{todos}'
        },
        plugins: [{
            type: 'multiselection',
            selectionColumn: {
                hidden: !1,
                width: 40
            }
        }],
        columns: [{
            text: 'Task',
            flex: 1,
            dataIndex: 'task'
        }]
    }, {
        xtype: 'toolbar',
        padding: '10 0 0 0',
        items: [{
            xtype: 'textfield',
            flex: 1,
            fieldLabel: 'Add Task',
            hideLabel: !0,
            placeholder: 'Add New Task'
        }, {
            xtype: 'button',
            ui: 'soft-green',
            iconCls: 'x-fa fa-plus',
            margin: '0 0 0 10'
        }]
    }]
});