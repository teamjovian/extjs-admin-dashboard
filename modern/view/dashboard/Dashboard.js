Ext.define('Admin.view.dashboard.Dashboard', {
    extend: 'Ext.Container',
    alias: 'widget.admindashboard',
    // xtype: 'admindashboard',

    requires: [
        'Shared.view.dashboard.DashboardController',
        'Shared.view.dashboard.DashboardModel',

        'Admin.view.dashboard.Earnings',
        'Admin.view.dashboard.HDDUsage',
        'Admin.view.dashboard.Network',
        'Admin.view.dashboard.Sales',
        'Admin.view.dashboard.Services',
        'Admin.view.dashboard.Todos',
        'Admin.view.dashboard.TopMovies',
        'Shared.view.dashboard.Weather'
    ],

    controller: 'dashboard',
    viewModel: {
        type: 'dashboard'
    },

    cls: 'dashboard',
    scrollable: !0,

    items: [{
        xtype: 'network',
        userCls: 'big-60 small-100 dashboard-item shadow'
    }, {
        xtype: 'hddusage',
        userCls: 'big-20 small-50 dashboard-item shadow'
    }, {
        xtype: 'earnings',
        userCls: 'big-20 small-50 dashboard-item shadow'
    }, {
        xtype: 'sales',
        userCls: 'big-20 small-50 dashboard-item shadow'
    }, {
        xtype: 'topmovies',
        userCls: 'big-20 small-50 dashboard-item shadow'
    }, {
        xtype: 'weather',
        userCls: 'big-40 small-100 dashboard-item shadow'
    }, {
        xtype: 'todo',
        height: 340,
        userCls: 'big-60 small-100 dashboard-item shadow'
    }, {
        xtype: 'services',
        height: 340,
        userCls: 'big-40 small-100 dashboard-item shadow'
    }]
});