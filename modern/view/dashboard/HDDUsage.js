Ext.define('Admin.view.dashboard.HDDUsage', {
    extend: 'Ext.Panel',
    alias: 'widget.hddusage',
    // xtype: 'hddusage',

    ui: 'light',
    height: 130,
    layout: 'fit',
    title: 'HDD Usage',
    cls: 'quick-graph-panel',
    header: {
        docked: 'bottom'
    },

    platformConfig: {
        '!phone': {
            iconCls: 'x-fa fa-database'
        }
    },

    items: [{
        xtype: 'cartesian',
        animation: !Ext.isIE9m && Ext.os.is.Desktop,
        downloadServerUrl: Shared.Config.downloadServerUrl,
        constrain: !0,
        constrainHeader: !0,
        background: '#70bf73',
        colors: ['#a9d9ab'],
        bind: {
            store: '{hddusage}'
        },
        axes: [{
            type: 'category',
            fields: ['xvalue'],
            hidden: !0,
            position: 'bottom'
        }, {
            type: 'numeric',
            fields: ['yvalue'],
            grid: {
                odd: {
                    fill: '#e8e8e8'
                }
            },
            hidden: !0,
            position: 'left'
        }],
        series: [{
            type: 'area',
            style: {
                stroke: '#FFFFFF',
                'stroke-width': '2px'
            },
            useDarkerStrokeColor: !1,
            xField: 'xvalue',
            yField: ['yvalue']
        }],
        interactions: [{
            type: 'panzoom'
        }]
    }]
});