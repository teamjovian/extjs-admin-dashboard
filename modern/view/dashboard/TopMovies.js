Ext.define('Admin.view.dashboard.TopMovies', {
    extend: 'Ext.Panel',
    alias: 'widget.topmovies',
    // xtype: 'topmovies',

    ui: 'light',
    height: 130,
    layout: 'fit',
    title: 'Top Movie',
    cls: 'quick-graph-panel',
    header: {
        docked: 'bottom'
    },
    platformConfig: {
        '!phone': {
            iconCls: 'x-fa fa-video-camera'
        }
    },
    items: [{
        xtype: 'polar',
        animation: !Ext.isIE9m && Ext.os.is.Desktop,
        downloadServerUrl: Shared.Config.downloadServerUrl,
        background: '#33abaa',
        colors: ['#115fa6', '#94ae0a', '#a61120', '#ff8809', '#ffd13e', '#a61187', '#24ad9a', '#7c7474', '#a66111'],
        radius: 100,
        bind: '{topMovies}',
        series: [{
            type: 'pie',
            colors: ['#ffffff'],
            label: {
                field: 'x',
                display: 'rotate',
                contrast: !0,
                font: '12px Arial'
            },
            xField: 'yvalue'
        }],
        interactions: [{
            type: 'rotate'
        }]
    }]
});