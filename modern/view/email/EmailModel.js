Ext.define('Admin.view.email.EmailModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.email',

    requires: [
        'Shared.store.email.Inbox',
        'Shared.store.email.Friends'
    ],

    stores: {
        inbox: { type: 'inbox' },
        friends: { type: 'emailfriends' }
    }
});