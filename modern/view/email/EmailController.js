Ext.define('Admin.view.email.EmailController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Admin.view.email.EmailModel',
        'Admin.view.email.Actions'
    ],

    actionsVisible: !1,

    onChangeFilter: function (a) {
        console.log('Show ' + a.getItemId())
    },

    onComposeMessage: function () {
        this.doCompose()
    },

    onComposeTo: function (a) {
        var b = a.getRecord();
        this.doCompose(b.get('name'))
    },

    onSelectMessage: function (b, a) {},

    hideActions: function () {
        var a = this.actions;
        if (a) {
            a.hide()
        }
        this.actionsVisible = !1
    },
    
    showActions: function () {
        var b = this,
        a = b.actions;
        if (!a) {
            console.info('fucking fuck up');
            b.actions = a = Ext.create({
                    xtype: 'emailactions',
                    defaults: {
                        scope: b
                    },
                    side: 'right',
                    hidden: !0,
                    hideOnMaskTap: !0,
                    width: 250
                });
            Ext.Viewport.add(a)
        }
        a.on('hide', function () {
            b.actionsVisible = !1
        }, null, {
            single: !0
        });
        a.show();
        b.actionsVisible = !0
    }
});