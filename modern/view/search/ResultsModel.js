Ext.define('Admin.view.search.ResultsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.searchresults',

    requires: [
        'Shared.store.search.Results',
        'Shared.store.search.Users',
        'Shared.store.email.Inbox'
    ],

    stores: {
        results: { type: 'searchresults' },
        users: { type: 'searchusers' },
        inbox: { type: 'inbox' }
    }
});