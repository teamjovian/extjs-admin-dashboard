Ext.define('Admin.view.search.Results', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.searchresults',
    // xtype: 'searchresults',

    requires: [
        'Admin.view.search.ResultsModel',
        'Admin.view.search.All',
    ],

    activeTab: 0,
    viewModel: {
        type: 'searchresults'
    },
    items: [{
        title: 'All',
        xtype: 'allresults',
        bind: {
            store: '{results}'
        }
    }, {
        title: 'Users',
        xtype: 'searchusers',
        bind: {
            store: '{users}'
        }
    }, {
        title: 'Messages',
        xtype: 'container',
        layout: 'fit',
        items: [{
                xtype: 'inbox',
                hideHeaders: !0,
                bind: '{inbox}'
            }
        ]
    }]
});