Ext.define('Admin.view.pages.ErrorBase', {
    extend: 'Ext.Container',

    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
    },
    cls: 'error-page-container',
    defaults: {
        xtype: 'label'
    }
});
