Ext.define('Admin.view.widgets.SocialBar', {
    extend: 'Ext.Toolbar',
    alias: 'widget.socialbar',
    // xtype: 'socialbar',

    border: !1,
    layout: {
        pack: 'center'
    },
    defaults: {
        border: !1,
        margin: '2 3'
    },
    items: [{
        ui: 'facebook',
        iconCls: 'x-fa fa-facebook',
        handler: function () {
            var a = this.getParent();
            a.fireEvent('facebook', a)
        }
    }, {
        ui: 'soft-cyan',
        iconCls: 'x-fa fa-twitter',
        handler: function () {
            var a = this.getParent();
            a.fireEvent('twitter', a)
        }
    }, {
        ui: 'soft-red',
        iconCls: 'x-fa fa-google-plus',
        handler: function () {
            var a = this.getParent();
            a.fireEvent('googleplus', a)
        }
    }, {
        ui: 'soft-purple',
        iconCls: 'x-fa fa-envelope',
        handler: function () {
            var a = this.getParent();
            a.fireEvent('email', a)
        }
    }]
});