Ext.define('Admin.view.widgets.BioTitle', {
    extend: 'Ext.Container',
    alias: 'widget.biotile',
    // xtype: 'biotile',

    cls: 'bio-tile',
    layout: 'fit',
    config: {
        banner: null,
        footer: null,
        image: null,
        description: null
    },
    referenceHolder: !0,
    items: [{
        xtype: 'container',
        layout: 'vbox',
        items: [{
            xtype: 'image',
            reference: 'banner',
            userCls: 'bio-banner',
            flex: 1,
            html: '&#160;'
        }, {
            xtype: 'container',
            reference: 'bottomContainer',
            layout: 'vbox',
            flex: 1,
            items: [{
                xtype: 'component',
                reference: 'description',
                userCls: 'bio-description',
                flex: 1
            }]
        }]
    }, {
        xtype: 'image',
        reference: 'image',
        left: '50%',
        top: '50%',
        userCls: 'bio-image'
    }],

    updateBanner: function (a) {
        this.configureImage('banner', a)
    },

    updateDescription: function (b) {
        var a = this.lookup('description');
        a.setHtml(b)
    },

    updateImage: function (a) {
        this.configureImage('image', a)
    },

    updateFooter: function (e) {
        var b = this.lookup('bottomContainer'),
        c = b.getItems(),
        d = c.length > 1 ? c.getAt(1) : null,
        a;
        a = Ext.factory(e, null, d);
        if (a !== d) {
            if (a) {
                b.add(a)
            }
        }
    },

    privates: {
        configureImage: function (reference, source) {
            var component = this.lookup(reference),
                config = source;

            if (typeof source === 'string') config = { src: source }
            component.setConfig(config);
        }
    }
});