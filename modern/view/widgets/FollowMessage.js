Ext.define('Admin.view.widgets.FollowMessage', {
    extend: 'Ext.Toolbar',
    alias: 'widget.followmessage',
    // xtype: 'followmessage',

    border: !1,
    layout: {
        pack: 'center'
    },
    defaults: {
        margin: '2 6'
    },
    items: [{
        text: 'Follow',
        ui: 'soft-green',
        handler: function () {
            var a = this.getParent();
            a.fireEvent('follow', a)
        }
    }, {
        text: 'Message',
        ui: 'soft-blue',
        handler: function () {
            var a = this.getParent();
            a.fireEvent('message', a)
        }
    }]
});