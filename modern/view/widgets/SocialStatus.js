Ext.define('Admin.view.widgets.SocialStatus', {
    extend: 'Ext.Toolbar',
    alias: 'widget.socialstatus',
    xtype: 'socialstatus',

    border: !1,
    config: {
        format: '0,000',
        followers: null,
        following: null,
        likes: null
    },
    referenceHolder: !0,
    items: [{
        xtype: 'component',
        flex: 1,
        reference: 'following',
        userCls: 'social-status-category',
        html: '<div class="social-status-header">Following</div><div class="social-status-count">3</div>'
    }, {
        xtype: 'component',
        flex: 1,
        reference: 'followers',
        userCls: 'social-status-category',
        html: '<div class="social-status-header">Followers</div><div class="social-status-count">1</div>'
    }, {
        xtype: 'component',
        flex: 1,
        reference: 'likes',
        userCls: 'social-status-category',
        html: '<div class="social-status-header">Likes</div><div class="social-status-count">2</div>'
    }],

    updateFollowers: function (a) {
        this.setCount('followers', a)
    },

    updateFollowing: function (a) {
        this.setCount('following', a)
    },

    updateLikes: function (a) {
        this.setCount('likes', a)
    },

    privates: {
        setCount: function (d, c) {
            var a = this.lookup(d),
            e = a.element.down('.social-status-count'),
            b = this.getFormat();
            e.dom.textContent = Ext.util.Format.number(c, b)
        }
    }
});