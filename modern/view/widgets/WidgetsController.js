Ext.define('Admin.view.widgets.WidgetsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.widgets',

    onFollow: function (a) {},
    onMessage: function (a) {},
    onContactFacebook: function (a) {},
    onContactTwitter: function (a) {},
    onContactGooglePlus: function (a) {},
    onContactEmail: function (a) {}
});