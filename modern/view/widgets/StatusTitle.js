Ext.define('Admin.view.widgets.StatusTitle', {
    extend: 'Ext.Component',
    alias: 'widget.statustile',
    // xtype: 'statustile',

    config: {
        format: '0,000',
        color: null,
        description: null,
        iconCls: null,
        iconFirst: !1,
        quantity: null,
        scale: null
    },

    element: {
        reference: 'element',
        cls: 'status-tile',
        children: [{
            cls: 'status-tile-wrap',
            children: [{
                reference: 'quantityElement',
                cls: 'status-tile-quantity'
            }, {
                reference: 'descriptionElement',
                cls: 'status-tile-description'
            }, {
                cls: 'status-tile-icon-wrap',
                reference: 'iconWrapElement',
                children: [{
                    reference: 'iconElement',
                    cls: 'status-tile-icon'
                }]
            }]
        }]
    },

    updateColor: function (a) {
        this.element.setStyle('borderTopColor', a);
        this.syncIconBackground()
    },

    updateDescription: function (a) {
        this.setText('descriptionElement', a)
    },

    updateIconCls: function (b, a) {
        this.iconElement.replaceCls(a, b)
    },

    updateIconFirst: function (b) {
        var c = this.iconElement.dom.parentNode,
        a = this.quantityElement.dom;
        a.parentNode.insertBefore(c, b ? a : null);
        this.element.toggleCls('status-tile-icon-first', b);
        this.syncIconBackground()
    },

    updateQuantity: function (b) {
        var a = this.getFormat(),
        c = Ext.util.Format.number(b, a);
        this.setText('quantityElement', c)
    },

    updateScale: function (c, b) {
        var a = this,
        d = a.getScaleCls(b),
        e = a.getScaleCls(c);
        a.element.replaceCls(d, e)
    },

    privates: {
        getScaleCls: function (a) {
            return a ? 'status-tile-' + a : ''
        },

        setText: function (b, a) {
            this[b].dom.textContent = a
        },

        syncIconBackground: function () {
            var a = '';
            if (this.getIconFirst()) {
                a = this.getColor()
            }
            this.iconWrapElement.setStyle('backgroundColor', a)
        }
    }
});