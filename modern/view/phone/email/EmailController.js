Ext.define('Admin.view.phone.email.EmailController', {
    extend: 'Admin.view.email.EmailController',
    alias: 'controller.email-phone',

    closeComposer: function () {
        var a = this,
        b = a.composer,
        d = a.getView(),
        c = a.getViewModel();
        if (b) {
            d.remove(b);
            a.composer = null;
            c.set('composing', !1)
        }
    },
    onPlusButtonTap: function () {
        if (!this.actionsVisible) {
            this.doCompose()
        }
    },
    doCompose: function (d) {
        var a = this,
        b = a.composer,
        f = a.getView(),
        e = a.getViewModel(),
        c;
        a.hideActions();
        if (!b) {
            a.composer = b = f.add({
                    xtype: 'compose',
                    flex: 1
                });
            if (d) {
                c = a.lookupReference('toField');
                c.setValue(d)
            }
            e.set('composing', !0)
        }
    },
    onChangeFilter: function (a) {
        this.hideActions();
        Admin.view.email.EmailController.prototype.onChangeFilter.apply(this, arguments)
    },
    onCloseMessage: function () {
        this.closeComposer()
    },
    onLongPressCompose: function (a) {
        this.showActions()
    },
    onSendMessage: function () {
        this.closeComposer()
    },
    onSwipe: function (a) {
        if (a.direction === 'left') {
            this.showActions()
        }
    }
});
