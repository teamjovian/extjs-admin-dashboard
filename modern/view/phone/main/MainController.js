Ext.define('Admin.view.phone.main.MainController', {
    extend: 'Admin.view.main.MainController',
    alias: 'controller.phone-main',

    slidOutCls: 'main-nav-slid-out',
    showNavigation: !1,

    init: function (d) {
        var b = this,
        e = b.getReferences(),
        c = e.logo,
        a;
        Admin.view.main.MainController.prototype.init.call(this, d);
        a = b.nav;
        a.getParent().remove(a, !1);
        a.addCls(['x-floating', 'main-nav-floated', b.slidOutCls]);
        a.setScrollable(!0);
        a.getRefOwner = function () {
            return d
        };
        a.add(c);
        c.setDocked('top');
        Ext.getBody().appendChild(a.element)
    },

    onNavigationItemClick: function (b, a) {
        if (a.select) {
            this.setShowNavigation(!1)
        }
    },

    onNavigationTreeSelectionChange: function (b, a) {
        this.setShowNavigation(!1);
        Admin.view.main.MainController.prototype.onNavigationTreeSelectionChange.apply(this, arguments)
    },

    updateShowNavigation: function (c, d) {
        if (d !== undefined) {
            var a = this,
            e = a.nav,
            b = a.mask;
            if (c) {
                a.mask = b = Ext.Viewport.add({
                        xtype: 'loadmask',
                        userCls: 'main-nav-mask'
                    });
                b.element.on({
                    tap: a.onToggleNavigationSize,
                    scope: a,
                    single: !0
                })
            } else {
                if (b) {
                    b.destroy();
                    a.mask = null
                }
            }
            e.toggleCls(a.slidOutCls, !c)
        }
    }
});
