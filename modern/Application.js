Ext.define('Admin.Application', {
    extend: 'Ext.app.Application',

    requires: [
        'Shared.proxy.API'
    ],

    controllers: [
        'Admin'
    ],
    
    name: 'Admin',
    stores: ['NavigationTree'],
    appFolder: 'modern',
    defaultToken: 'dashboard',
    mainView: 'Admin.view.main.Main',
    profiles: ['Phone', 'Tablet'],
    stores: ['NavigationTree']
});