Ext.define('Admin.profile.Phone', {
    extend: 'Ext.app.Profile',

    requires: [
        'Admin.view.phone.main.MainController'
    ],

    views: {
        email: 'Admin.view.phone.email.Email',
        inbox: 'Admin.view.phone.email.Inbox',
        compose: 'Admin.view.phone.email.Compose',
        searchusers: 'Admin.view.phone.search.Users'
    },

    isActive: function () {
        return Ext.platformTags.phone
    },

    launch: function () {
        Ext.getBody().addCls('phone')
    }
});